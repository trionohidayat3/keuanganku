/*
 * Keuanganku
 * ExampleUnitTest.java
 *
 * Created by Triono Hidayat on 4/20/22, 1:34 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/18/22, 2:05 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}