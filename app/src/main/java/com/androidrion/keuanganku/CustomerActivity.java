/*
 * Keuanganku
 * CustomerActivity.java
 *
 * Created by Triono Hidayat on 6/7/22, 5:30 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/7/22, 5:30 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends AppCompatActivity {

    //User Email
    SharedPrefManager sharedPrefManager;

    //API
    API_Interface apiInterface;

    //Adapter
    CustomerAdapter customerAdapter;

    //List
    List<ModelCustomer> modelCustomerList;

    //Component Activity
    RecyclerView rvCustomer;
    FloatingActionButton fabAddCustomer;

    //Component Dialog
    TextInputEditText inputName, inputEmail, inputPhone, inputAddress;
    MaterialCardView cardAdd;

    //String Dialog
    String sName, sEmail, sPhone, sAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Daftar Pelanggan");

        apiInterface = API_Client.getClient().create(API_Interface.class);

        sharedPrefManager = new SharedPrefManager(this);

        rvCustomer = findViewById(R.id.recycler_customer);
        rvCustomer.setHasFixedSize(true);
        rvCustomer.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvCustomer.setLayoutManager(linearLayoutManager);

        fabAddCustomer = findViewById(R.id.fab_add_customer);
        fabAddCustomer.setOnClickListener(view -> showDialogAddCustomer());

        Call<List<ModelCustomer>> viewCustomer = apiInterface.view_customer(sharedPrefManager.getSPEmail());
        viewCustomer.enqueue(new Callback<List<ModelCustomer>>() {
            @Override
            public void onResponse(Call<List<ModelCustomer>> call, Response<List<ModelCustomer>> response) {
                modelCustomerList = response.body();
                customerAdapter = new CustomerAdapter(modelCustomerList);
                rvCustomer.setAdapter(customerAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelCustomer>> call, Throwable t) {

            }
        });

    }

    private void showDialogAddCustomer() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        inputName = dialog.findViewById(R.id.input_cust_name);
        inputEmail = dialog.findViewById(R.id.input_cust_email);
        inputPhone = dialog.findViewById(R.id.input_cust_phone);
        inputAddress = dialog.findViewById(R.id.input_cust_address);

        cardAdd = dialog.findViewById(R.id.card_add);

        cardAdd.setOnClickListener(view -> {

            sName = inputName.getText().toString();
            sEmail = inputEmail.getText().toString();
            sPhone = inputPhone.getText().toString();
            sAddress = inputAddress.getText().toString();

            Call<ResponseBody> addCustomer = apiInterface.add_customer(sName, sEmail, sPhone, sAddress, sharedPrefManager.getSPEmail());
            addCustomer.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {

                        dialog.dismiss();
                    } else {
                        Toast.makeText(CustomerActivity.this, "Maaf, coba lagi!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_customer, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<ModelCustomer> itemFilter = new ArrayList<>();
                for (ModelCustomer model : modelCustomerList) {
                    String nama = model.getColCustomerName().toLowerCase();
                    if (nama.contains(newText)) {
                        itemFilter.add(model);
                    }
                }
                customerAdapter.setFilter(itemFilter);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}