/*
 * Keuanganku
 * RequestAdapter.java
 *
 * Created by Triono Hidayat on 4/21/22, 10:25 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/21/22, 10:25 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    ArrayList<ModelRequest> modelRequestArrayList;
    Context context;

    public RequestAdapter(ArrayList<ModelRequest> modelRequestArrayList, Context mContext) {
        this.modelRequestArrayList = modelRequestArrayList;
        this.context = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        ModelRequest modelRequest = modelRequestArrayList.get(position);

        holder.textReqDate.setText(modelRequest.getColDate());
        holder.textReqDesc.setText(modelRequest.getColDesc());
        holder.textReqAmount.setText(formatRupiah(Double.parseDouble(modelRequest.getColAmount())));

        API_Interface apiInterface = API_Client.getClient().create(API_Interface.class);

        holder.btnTake.setOnClickListener(view -> {

            Call<ResponseBody> callTransactionEwallet = apiInterface.add_transaction_wallet("in", modelRequest.getColAmount(), "E-wallet", String.valueOf(holder.textReqDesc.getText()), String.valueOf(holder.textReqDate.getText()), sharedPrefManager.getSPEmail());
            callTransactionEwallet.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

            holder.btnTake.setEnabled(false);
        });
    }

    @Override
    public int getItemCount() {
        return modelRequestArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textReqDate, textReqDesc, textReqAmount;
        Button btnTake;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textReqDate = itemView.findViewById(R.id.text_req_date);
            textReqDesc = itemView.findViewById(R.id.text_req_desc);
            textReqAmount = itemView.findViewById(R.id.text_req_amount);
            btnTake = itemView.findViewById(R.id.btn_take_it);
        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
