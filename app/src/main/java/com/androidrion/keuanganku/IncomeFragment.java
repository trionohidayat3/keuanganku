/*
 * Keuanganku
 * IncomeFragment.java
 *
 * Created by Triono Hidayat on 5/31/22, 9:58 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/31/22, 9:58 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IncomeFragment extends Fragment {

    SharedPrefManager sharedPrefManager;

    RecyclerView rvIncoming;
    API_Interface apiInterface;

    IncomeAdapter adapter;
    List<ModelTransaction> modelTransactions;

    public IncomeFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {

        Bundle args = new Bundle();

        IncomeFragment fragment = new IncomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_income, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPrefManager = new SharedPrefManager(getContext());


        rvIncoming = view.findViewById(R.id.rv_incoming);
        rvIncoming.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvIncoming.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        apiInterface = API_Client.getClient().create(API_Interface.class);

        Call<List<ModelTransaction>> getIncomeReport = apiInterface.view_report_transaction("in", sharedPrefManager.getSPEmail() );
        getIncomeReport.enqueue(new Callback<List<ModelTransaction>>() {
            @Override
            public void onResponse(Call<List<ModelTransaction>> call, Response<List<ModelTransaction>> response) {
                modelTransactions = response.body();
                adapter = new IncomeAdapter(modelTransactions);
                rvIncoming.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelTransaction>> call, Throwable t) {

            }
        });

    }
}