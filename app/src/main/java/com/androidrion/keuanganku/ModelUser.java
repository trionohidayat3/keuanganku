/*
 * Keuanganku
 * ModelUser.java
 *
 * Created by Triono Hidayat on 4/20/22, 1:34 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/18/22, 5:43 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelUser {

    @SerializedName("col_name")
    @Expose
    private String colName;
    @SerializedName("col_email")
    @Expose
    private String colEmail;
    @SerializedName("col_password")
    @Expose
    private String colPassword;
    @SerializedName("col_phone_number")
    @Expose
    private String colPhoneNumber;
    @SerializedName("col_address")
    @Expose
    private String colAddress;
    @SerializedName("col_photo_profile")
    @Expose
    private String colPhotoProfile;
    @SerializedName("col_date_create")
    @Expose
    private Object colDateCreate;
    @SerializedName("col_date_update")
    @Expose
    private Object colDateUpdate;


    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColEmail() {
        return colEmail;
    }

    public void setColEmail(String colEmail) {
        this.colEmail = colEmail;
    }

    public String getColPassword() {
        return colPassword;
    }

    public void setColPassword(String colPassword) {
        this.colPassword = colPassword;
    }

    public String getColPhoneNumber() {
        return colPhoneNumber;
    }

    public void setColPhoneNumber(String colPhoneNumber) {
        this.colPhoneNumber = colPhoneNumber;
    }

    public String getColAddress() {
        return colAddress;
    }

    public void setColAddress(String colAddress) {
        this.colAddress = colAddress;
    }

    public String getColPhotoProfile() {
        return colPhotoProfile;
    }

    public void setColPhotoProfile(String colPhotoProfile) {
        this.colPhotoProfile = colPhotoProfile;
    }

    public Object getColDateCreate() {
        return colDateCreate;
    }

    public void setColDateCreate(Object colDateCreate) {
        this.colDateCreate = colDateCreate;
    }

    public Object getColDateUpdate() {
        return colDateUpdate;
    }

    public void setColDateUpdate(Object colDateUpdate) {
        this.colDateUpdate = colDateUpdate;
    }

}