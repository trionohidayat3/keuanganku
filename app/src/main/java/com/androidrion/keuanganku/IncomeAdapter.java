/*
 * Keuanganku
 * IncomeAdapter.java
 *
 * Created by Triono Hidayat on 5/31/22, 1:09 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/31/22, 1:09 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class IncomeAdapter extends RecyclerView.Adapter<IncomeAdapter.ViewHolder> {

    List<ModelTransaction> modelTransactionList;

    public IncomeAdapter(List<ModelTransaction> modelTransactionList) {
        this.modelTransactionList = modelTransactionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_income, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelTransaction modelTransaction = modelTransactionList.get(position);

        holder.textType.setText(modelTransaction.getColType());
        holder.textTotalAmount.setText(modelTransaction.getColTotalAmount());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textType, textTotalAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textType = itemView.findViewById(R.id.text_type);
            textTotalAmount = itemView.findViewById(R.id.text_total_amount);
        }
    }
}
