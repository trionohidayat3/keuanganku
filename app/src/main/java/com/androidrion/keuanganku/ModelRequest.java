/*
 * Keuanganku
 * ModelRequest.java
 *
 * Created by Triono Hidayat on 4/21/22, 10:16 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/21/22, 10:16 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelRequest {

    @SerializedName("col_date")
    @Expose
    private String colDate;
    @SerializedName("col_desc")
    @Expose
    private String colDesc;
    @SerializedName("col_amount")
    @Expose
    private String colAmount;

    public String getColDate() {
        return colDate;
    }

    public void setColDate(String colDate) {
        this.colDate = colDate;
    }

    public String getColDesc() {
        return colDesc;
    }

    public void setColDesc(String colDesc) {
        this.colDesc = colDesc;
    }

    public String getColAmount() {
        return colAmount;
    }

    public void setColAmount(String colAmount) {
        this.colAmount = colAmount;
    }

}