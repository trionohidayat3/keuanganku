/*
 * Keuanganku
 * PiutangActivity.java
 *
 * Created by Triono Hidayat on 6/6/22, 8:43 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/6/22, 8:43 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClaimActivity extends AppCompatActivity {

    //User Email
    SharedPrefManager sharedPrefManager;

    Calendar calendar;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat;

    //API
    API_Interface apiInterface;

    //Activity Component
    SwipeRefreshLayout refreshLayout;
    TextView textDebt;
    RecyclerView rvDebt;
    FloatingActionButton fabAddDebt, fabAddDebtPerson, fabAddDebtNote;
    View backDrop, lytAddDebtPerson, lytAddDebtNote;

    //Dialog Add Debt Note Component
    MaterialButtonToggleGroup toggleDebt;
    MaterialButton btnOfferDebt, btnGetDebt;
    AutoCompleteTextView autoDebtName, autoBankAccount;
    TextInputLayout layoutDebtName;
    RadioGroup rgSaving;
    RadioButton rbKas, rbBank;
    TextInputLayout layoutBank;
    TextInputEditText inputDebtAmount, inputDebtDate, inputDebtDateDue, inputDebtNote;
    MaterialCardView cardDebtSave;

    //Dialog Add Debt Person Component
    TextInputEditText inputDebtPerson;
    ImageView ivAddPerson;
    RecyclerView rvListPerson;

    //List
//    List<ModelTransactionDebt> transactionDebts;
    List<ModelCustomer> customers;
    ArrayAdapter<String> adapter;
    List<ModelCard> modelCards;

    //Adapter Activity
    TransactionDebtMeAdapter debtAdapter;

    //Adapter Dialog
    CustomerAdapter customerAdapter;
    DebtBankAdapter debtBankAdapter;


    //String Dialog Component
    String sType, sPerson, sBankName, sAmount, sDate, sDateDue, sNote;

    String rupiah = "0";

    private boolean rotate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Catatan Piutang");

        sharedPrefManager = new SharedPrefManager(this);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        apiInterface = API_Client.getClient().create(API_Interface.class);

        refreshLayout = findViewById(R.id.swipe_refresh);
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshLayout.setOnRefreshListener(() -> {
            refreshLayout.setRefreshing(false);
            loadData();
        });

        textDebt = findViewById(R.id.text_debt);
        textDebt.setText(formatRupiah(Double.parseDouble(rupiah)));

        rvDebt = findViewById(R.id.recycler_debt);
        rvDebt.setHasFixedSize(true);
        rvDebt.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvDebt.setLayoutManager(linearLayoutManager);

        fabAddDebt = findViewById(R.id.fab_add_debt);
        fabAddDebtPerson = findViewById(R.id.fab_add_debt_person);
        fabAddDebtNote = findViewById(R.id.fab_add_debt_note);

        backDrop = findViewById(R.id.back_drop);
        lytAddDebtPerson = findViewById(R.id.lyt_add_debt_person);
        lytAddDebtNote = findViewById(R.id.lyt_add_debt_note);
        ViewAnimation.initShowOut(lytAddDebtPerson);
        ViewAnimation.initShowOut(lytAddDebtNote);
        backDrop.setVisibility(View.GONE);

        fabAddDebt.setOnClickListener(view -> toggleFabMode(view));

        fabAddDebtPerson.setOnClickListener(view -> showDialogAddPerson());
        fabAddDebtNote.setOnClickListener(view -> showDialogDebt());

        backDrop.setOnClickListener(v -> toggleFabMode(fabAddDebt));

        loadData();
    }

    private void loadData() {

        Call<List<ModelTransactionDebt>> callTotalDebt = apiInterface.view_total_debt(sharedPrefManager.getSPEmail());
        callTotalDebt.enqueue(new Callback<List<ModelTransactionDebt>>() {
            @Override
            public void onResponse(Call<List<ModelTransactionDebt>> call, Response<List<ModelTransactionDebt>> response) {
//                transactionDebts = response.body();
//                List<String> listAuto = new ArrayList<>();
//                for (int i = 0; i < Objects.requireNonNull(transactionDebts).size(); i++) {
//                    listAuto.add(transactionDebts.get(i).getColUserEmail());
//                    String sTotalCash = transactionDebts.get(i).getTotalAmount();
//
//                    if (transactionDebts.get(i).getColType().equals("in")) {
//                        textDebt.setText(formatRupiah(Double.parseDouble(sTotalCash)));
//                    }
//                }
            }

            @Override
            public void onFailure(Call<List<ModelTransactionDebt>> call, Throwable t) {

            }
        });

        Call<List<ModelTransactionDebt>> callTransDebt = apiInterface.view_total_debt_specific("in", sharedPrefManager.getSPEmail());
        callTransDebt.enqueue(new Callback<List<ModelTransactionDebt>>() {
            @Override
            public void onResponse(Call<List<ModelTransactionDebt>> call, Response<List<ModelTransactionDebt>> response) {
//                transactionDebts = response.body();
//                debtAdapter = new TransactionDebtAdapter(transactionDebts, getApplicationContext());
//                rvDebt.setAdapter(debtAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelTransactionDebt>> call, Throwable t) {

            }
        });
    }

    private void toggleFabMode(View view) {
        rotate = ViewAnimation.rotateFab(view, !rotate);
        if (rotate) {
            ViewAnimation.showIn(lytAddDebtPerson);
            ViewAnimation.showIn(lytAddDebtNote);
            backDrop.setVisibility(View.VISIBLE);
        } else {
            ViewAnimation.showOut(lytAddDebtPerson);
            ViewAnimation.showOut(lytAddDebtNote);
            backDrop.setVisibility(View.GONE);
        }
    }

    private void showDialogAddPerson() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

//        inputDebtPerson = dialog.findViewById(R.id.input_add_person);
//        ivAddPerson = dialog.findViewById(R.id.iv_add_person);
//        rvListPerson = dialog.findViewById(R.id.recycler_person);
//
//        rvListPerson.setHasFixedSize(true);
//        rvListPerson.setLayoutManager(new LinearLayoutManager(this));
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        rvListPerson.setLayoutManager(linearLayoutManager);
//
//        ivAddPerson.setOnClickListener(view -> {
//            String sName = inputDebtPerson.getText().toString();
//
//            Call<ResponseBody> callAddPerson = apiInterface.add_customer(sName, sharedPrefManager.getSPEmail());
//            callAddPerson.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    inputDebtPerson.setText("");
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                }
//            });
//
//            dialog.dismiss();
//        });
//
//        Call<List<ModelCustomer>> callViewCustomer = apiInterface.view_customer(sharedPrefManager.getSPEmail());
//        callViewCustomer.enqueue(new Callback<List<ModelCustomer>>() {
//            @Override
//            public void onResponse(Call<List<ModelCustomer>> call, Response<List<ModelCustomer>> response) {
//                customers = response.body();
//                customerAdapter = new CustomerAdapter(customers);
//                rvListPerson.setAdapter(customerAdapter);
//            }
//
//            @Override
//            public void onFailure(Call<List<ModelCustomer>> call, Throwable t) {
//
//            }
//        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogDebt() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        autoDebtName = dialog.findViewById(R.id.input_debt_name);
        rgSaving = dialog.findViewById(R.id.group_saving);
        rbKas = dialog.findViewById(R.id.radio_kas);
        rbBank = dialog.findViewById(R.id.radio_bank);
        layoutBank = dialog.findViewById(R.id.layout_bank);
        autoBankAccount = dialog.findViewById(R.id.auto_bank_account);
        inputDebtAmount = dialog.findViewById(R.id.input_debt_amount);
        inputDebtDate = dialog.findViewById(R.id.input_debt_date);
        inputDebtDateDue = dialog.findViewById(R.id.input_debt_due_date);
        inputDebtNote = dialog.findViewById(R.id.input_debt_note);
        cardDebtSave = dialog.findViewById(R.id.card_debt_save);

        Call<List<ModelCustomer>> callViewCustomer = apiInterface.view_customer(sharedPrefManager.getSPEmail());
        callViewCustomer.enqueue(new Callback<List<ModelCustomer>>() {
            @Override
            public void onResponse(Call<List<ModelCustomer>> call, Response<List<ModelCustomer>> response) {
                customers = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(customers).size(); i++) {
                    listAuto.add(customers.get(i).getColCustomerName());

                }

                adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoDebtName.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelCustomer>> call, Throwable t) {

            }
        });

        rgSaving.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSaving.getCheckedRadioButtonId();

            if (selectedId == rbKas.getId()) {
                layoutBank.setVisibility(View.GONE);
            } else if (selectedId == rbBank.getId()) {
                layoutBank.setVisibility(View.VISIBLE);
            } else {
                layoutBank.setVisibility(View.VISIBLE);
            }

            Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
            callCardUser.enqueue(new Callback<List<ModelCard>>() {
                @Override
                public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                    modelCards = response.body();
                    debtBankAdapter = new DebtBankAdapter(getApplicationContext(), modelCards);
                    autoBankAccount.setAdapter(debtBankAdapter);
                    autoBankAccount.setOnItemClickListener((adapterView, view, i, l) -> {
                        ModelCard card = (ModelCard) adapterView.getItemAtPosition(i);

                        autoBankAccount.setText(card.getColNameBank());
                    });
                }

                @Override
                public void onFailure(Call<List<ModelCard>> call, Throwable t) {

                }
            });

        });

        inputDebtDate.setOnClickListener(view -> showDialogDate());
        inputDebtDateDue.setOnClickListener(view -> showDialogDateDue());

        cardDebtSave.setOnClickListener(view -> {

            sPerson = autoDebtName.getText().toString();
            sBankName = autoBankAccount.getText().toString();
            sAmount = inputDebtAmount.getText().toString();
            sDate = inputDebtDate.getText().toString();
            sDateDue = inputDebtDateDue.getText().toString();
            sNote = inputDebtNote.getText().toString();

            if (rbKas.isChecked()) {
                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("in", "Hutang", sPerson, "Kas", "-", sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("in", sAmount, "Kas", "Transaksi Utang", sDate, sharedPrefManager.getSPEmail());
                callTransactionKas.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            } else if (rbBank.isChecked()) {

                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("in", "Hutang", sPerson, "Bank", sBankName, sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                Call<ResponseBody> callTransactionBank = apiInterface.add_transaction_bank("in", sAmount, "Bank", sBankName, "Transaksi Hutang", sDate, sharedPrefManager.getSPEmail());
                callTransactionBank.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }

            dialog.dismiss();
        });


        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

    private void showDialogDate() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDate.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void showDialogDateDue() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDateDue.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}