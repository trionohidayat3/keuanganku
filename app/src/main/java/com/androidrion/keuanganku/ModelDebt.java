/*
 * Keuanganku
 * ModelDebt.java
 *
 * Created by Triono Hidayat on 6/9/22, 2:07 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/9/22, 2:07 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ModelDebt {

    @SerializedName("status")
    @Expose
    public Integer status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public List<Datum> data = new ArrayList<>();

    public static class Datum {

        @SerializedName("id")
        @Expose
        public String id;

        @SerializedName("debtor")
        @Expose
        public String debtor;

        @SerializedName("creditor")
        @Expose
        public String creditor;
        @SerializedName("amount")
        @Expose
        public String amount;
        @SerializedName("note")
        @Expose
        public String note;
        @SerializedName("due_date")
        @Expose
        public String dueDate;
        @SerializedName("start_date")
        @Expose
        public String startDate;
        @SerializedName("created_by")
        @Expose
        public String createdBy;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDebtor() {
            return debtor;
        }

        public void setDebtor(String debtor) {
            this.debtor = debtor;
        }

        public String getCreditor() {
            return creditor;
        }

        public void setCreditor(String creditor) {
            this.creditor = creditor;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}