/*
 * Keuanganku
 * DeptCustomerFragment.java
 *
 * Created by Triono Hidayat on 6/9/22, 1:20 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/9/22, 1:20 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeptCustomerFragment extends Fragment {

    //User Email
    SharedPrefManager sharedPrefManager;

    //API
    API_Interface apiInterface;

    //Component Fragment
    TextView textTotalDebtCust;
    RecyclerView rvDebtCust;

    //List
//    List<ModelTransactionDebt> transactionDebts;

    //Adapter
    TransactionDebtCustAdapter debtAdapter;

    //Format Rupiah
    String rupiah = "0";

    public DeptCustomerFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dept_customer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPrefManager = new SharedPrefManager(requireContext());

        apiInterface = API_Client.getClient().create(API_Interface.class);

        textTotalDebtCust = view.findViewById(R.id.text_total_debt_cust);
        rvDebtCust = view.findViewById(R.id.recycler_debt_cust);

        rvDebtCust.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvDebtCust.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        Call<ModelDebt> viewTotalAmountDebt = apiInterface.view_total_amount_debt(sharedPrefManager.getSPEmail(), sharedPrefManager.getSPEmail(), "", "", "0");
        viewTotalAmountDebt.enqueue(new Callback<ModelDebt>() {
            @Override
            public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                if (response.isSuccessful()) {
                    ModelDebt modelDebt = response.body();
                    List<ModelDebt.Datum> datumList = modelDebt.data;

                    for (int i = 0; i < Objects.requireNonNull(datumList).size(); i++) {
                        textTotalDebtCust.setText(formatRupiah(Double.parseDouble(datumList.get(i).amount)));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelDebt> call, @NonNull Throwable t) {

            }
        });

        Call<ModelDebt> viewDebtMe = apiInterface.view_list_debt(sharedPrefManager.getSPEmail(), "", "", sharedPrefManager.getSPEmail(), "0");
        viewDebtMe.enqueue(new Callback<ModelDebt>() {
            @Override
            public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                if (response.isSuccessful()) {
                    ModelDebt apigsonList = response.body();
                    assert apigsonList != null;
                    List<ModelDebt.Datum> datumList = apigsonList.data;

                    debtAdapter = new TransactionDebtCustAdapter(datumList, getContext());
                    rvDebtCust.setAdapter(debtAdapter);


                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelDebt> call, @NonNull Throwable t) {

            }
        });
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}