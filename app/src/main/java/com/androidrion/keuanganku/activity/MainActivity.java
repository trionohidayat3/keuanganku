/*
 * Keuanganku
 * MainActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/17/22, 3:07 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ClaimActivity;
import com.androidrion.keuanganku.CustomerActivity;
import com.androidrion.keuanganku.DebtActivity;
import com.androidrion.keuanganku.JurnalActivity;
import com.androidrion.keuanganku.ModelCard;
import com.androidrion.keuanganku.ModelTotalBank;
import com.androidrion.keuanganku.ModelTotalCash;
import com.androidrion.keuanganku.ModelTotalWallet;
import com.androidrion.keuanganku.ModelTransaction;
import com.androidrion.keuanganku.ModelUser;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.ReportActivity;
import com.androidrion.keuanganku.SharedPrefManager;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    MaterialToolbar mToolbar;
    TextView textBalance, textEwallet, textAmountBank;
    ImageView imgBalance;
    SwipeRefreshLayout swipeRefresh;
    FloatingActionButton fabPay, fabTopUp, fabRequest, fabAccountBank, fabReport, fabHistory, fabJurnal, fabSetting,
            fabCustomer, fabDebtNote, fabClaim;
    ProgressDialog progressDialog;

    private ViewPager viewPager;
    private LinearLayout layout_dots;

    RadioGroup rgSavingAdd, rgSavingPay;
    RadioButton rbKasAdd, rbBankAdd, rbKasPay, rbBankPay;

    TextInputLayout layoutBankAdd, layoutBankPay;
    AutoCompleteTextView autoBankAccountAdd, autoBankAccountPay;
    TextInputEditText txtDateAdd, txtAmountCashAdd, txtNoteAdd, txtDatePay, txtAmountCashPay, txtNotePay;

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat, dateFormatText;
    Calendar calendar;

    API_Interface apiInterface;
    private long exitTime = 0;

//    private View notif_badge;
//    private int notification_count = -1;

    List<ModelTransaction> modelTransactionList;
    List<ModelUser> modelUsers;
    List<ModelCard> modelCards;
    ArrayList<ModelTotalCash> modelTotalCashes;
    ArrayList<ModelTotalWallet> modelTotalWallets;
    ArrayList<ModelTotalBank> modelTotalBanks;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateFormatText = new SimpleDateFormat("dd-MM-yyyy");

        sharedPrefManager = new SharedPrefManager(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        progressDialog.show();

        apiInterface = API_Client.getClient().create(API_Interface.class);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        //Belum Selesai
        String rupiah = "0";

        textBalance = findViewById(R.id.text_balance);
        textBalance.setText(formatRupiah(Double.parseDouble(rupiah)));

        textEwallet = findViewById(R.id.text_amount_ewalet);
        textEwallet.setText(formatRupiah(Double.parseDouble(rupiah)));

        textAmountBank = findViewById(R.id.text_amount_bank);
        textAmountBank.setText(formatRupiah(Double.parseDouble(rupiah)));

        imgBalance = findViewById(R.id.img_balance);
        imgBalance.setOnClickListener(view -> startActivity(new Intent(this, BalanceActivity.class)));

        swipeRefresh = findViewById(R.id.swiperefresh);
        swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.setRefreshing(false);
            loadData();
        });

        fabPay = findViewById(R.id.fab_pay);
        fabPay.setOnClickListener(view -> {
            if (textBalance.getText().equals("Rp0,00")) {
                Toast.makeText(this, "Kas Anda kosong, Silahkan isi kas", Toast.LENGTH_SHORT).show();
            } else {
                openDialogPay();
            }
        });

        fabTopUp = findViewById(R.id.fab_top_up);
        fabTopUp.setOnClickListener(view -> openDialogTopUp());

        fabRequest = findViewById(R.id.fab_request);
        fabRequest.setOnClickListener(view -> startActivity(new Intent(this, RequestActivity.class)));

        fabAccountBank = findViewById(R.id.fab_account_bank);
        fabAccountBank.setOnClickListener(view -> startActivity(new Intent(this, AccountBankActivity.class)));

        fabReport = findViewById(R.id.fab_report);
        fabReport.setOnClickListener(view -> startActivity(new Intent(this, ReportActivity.class)));

        fabHistory = findViewById(R.id.fab_history);
        fabHistory.setOnClickListener(view -> startActivity(new Intent(this, HistoryActivity.class)));

        fabJurnal = findViewById(R.id.fab_jurnal);
        fabJurnal.setOnClickListener(view -> startActivity(new Intent(this, JurnalActivity.class)));

        fabSetting = findViewById(R.id.fab_setting);
        fabSetting.setOnClickListener(view -> startActivity(new Intent(this, SettingActivity.class)));

        fabCustomer = findViewById(R.id.fab_debt_customer);
        fabCustomer.setOnClickListener(view -> startActivity(new Intent(this, CustomerActivity.class)));

        fabDebtNote = findViewById(R.id.fab_debt_note);
        fabDebtNote.setOnClickListener(view -> startActivity(new Intent(this, DebtActivity.class)));

        fabClaim = findViewById(R.id.fab_debt_claim_note);
        fabClaim.setOnClickListener(view -> startActivity(new Intent(this, ClaimActivity.class)));

        loadData();

    }

    private void openDialogTopUp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_balance);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        layoutBankAdd = dialog.findViewById(R.id.layout_bank);
        autoBankAccountAdd = dialog.findViewById(R.id.auto_bank_account);

        rgSavingAdd = dialog.findViewById(R.id.group_saving);
        rbKasAdd = dialog.findViewById(R.id.radio_kas);
        rbBankAdd = dialog.findViewById(R.id.radio_bank);

        rgSavingAdd.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSavingAdd.getCheckedRadioButtonId();

            if (selectedId == rbKasAdd.getId()) {
                layoutBankAdd.setVisibility(View.GONE);
            } else if (selectedId == rbBankAdd.getId()) {
                layoutBankAdd.setVisibility(View.VISIBLE);
            } else {
                layoutBankAdd.setVisibility(View.VISIBLE);
            }
        });

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccountAdd.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        txtAmountCashAdd = dialog.findViewById(R.id.txt_amount_cash);
        txtDateAdd = dialog.findViewById(R.id.txt_add_date);
        txtDateAdd.setOnClickListener(view -> showDialogDateTopup());
        txtNoteAdd = dialog.findViewById(R.id.txt_amount_note);

        dialog.findViewById(R.id.bt_save).setOnClickListener(v -> {
            String sTanggal = txtDateAdd.getText().toString();
            String tanggal_jam = dateFormat.format(calendar.getTime());
            String sAmountCash = Objects.requireNonNull(txtAmountCashAdd.getText()).toString().trim();
            String sBankName = autoBankAccountAdd.getText().toString();
            String sNote = txtNoteAdd.getText().toString();

            if (rgSavingAdd.getCheckedRadioButtonId() == -1) {
                Toast.makeText(this, "Mohon Pilih Tabungan", Toast.LENGTH_SHORT).show();
            } else if (sAmountCash.isEmpty()) {
                Toast.makeText(this, "Mohon Masukkan Jumlah Uang", Toast.LENGTH_SHORT).show();
            } else if (sTanggal.isEmpty()) {
                Toast.makeText(this, "Mohon Masukkan Tanggal", Toast.LENGTH_SHORT).show();
            } else {

                if (rbKasAdd.isChecked()) {
                    Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("in", sAmountCash, "Kas", sNote, tanggal_jam, sharedPrefManager.getSPEmail());
                    callTransactionKas.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                } else if (rbBankAdd.isChecked()) {
                    Call<ResponseBody> callTransactionBank = apiInterface.add_transaction_bank("in", sAmountCash, "Bank", sBankName, sNote, tanggal_jam, sharedPrefManager.getSPEmail());
                    callTransactionBank.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }

                dialog.dismiss();
                loadData();
            }

        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void openDialogPay() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_pay);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        layoutBankPay = dialog.findViewById(R.id.layout_bank);
        autoBankAccountPay = dialog.findViewById(R.id.auto_bank_account);

        rgSavingPay = dialog.findViewById(R.id.group_saving);
        rbKasPay = dialog.findViewById(R.id.radio_kas);
        rbBankPay = dialog.findViewById(R.id.radio_bank);

        rgSavingPay.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSavingPay.getCheckedRadioButtonId();

            if (selectedId == rbKasPay.getId()) {
                layoutBankPay.setVisibility(View.GONE);
            } else if (selectedId == rbBankPay.getId()) {
                layoutBankPay.setVisibility(View.VISIBLE);
            } else {
                layoutBankPay.setVisibility(View.VISIBLE);
            }
        });

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccountPay.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        txtAmountCashPay = dialog.findViewById(R.id.txt_amount_cash);
        txtDatePay = dialog.findViewById(R.id.txt_add_date);
        txtDatePay.setOnClickListener(view -> showDialogDatePay());
        txtNotePay = dialog.findViewById(R.id.txt_amount_note);

        dialog.findViewById(R.id.btn_pay).setOnClickListener(v -> {
            String sTanggal = txtDatePay.getText().toString();
//            String tanggal_jam = dateFormat.format(calendar.getTime());
            String sAmountCash = txtAmountCashPay.getText().toString();
            String sBankName = autoBankAccountPay.getText().toString();
            String sNote = txtNotePay.getText().toString();

            if (rgSavingPay.getCheckedRadioButtonId() == -1) {
                Toast.makeText(this, "Mohon Pilih Tabungan", Toast.LENGTH_SHORT).show();
            } else if (sAmountCash.isEmpty()) {
                Toast.makeText(this, "Mohon Masukkan Jumlah Uang", Toast.LENGTH_SHORT).show();
            } else if (sTanggal.isEmpty()) {
                Toast.makeText(this, "Mohon Masukkan Tanggal", Toast.LENGTH_SHORT).show();
            } else {

                if (rbKasPay.isChecked()) {
                    Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("out", sAmountCash, "Kas", sNote, sTanggal, sharedPrefManager.getSPEmail());
                    callTransactionKas.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                } else if (rbBankPay.isChecked()) {
                    Call<ResponseBody> callTransactionBank = apiInterface.add_transaction_bank("out", sAmountCash, "Bank", sBankName, sNote, sTanggal, sharedPrefManager.getSPEmail());
                    callTransactionBank.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }

                dialog.dismiss();
                loadData();
            }

        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogDateTopup() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            txtDateAdd.setText(dateFormatText.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void showDialogDatePay() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            txtDatePay.setText(dateFormatText.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void loadData() {

        //Call User
        Call<List<ModelUser>> listCall = apiInterface.user_view(sharedPrefManager.getSPEmail());
        listCall.enqueue(new Callback<List<ModelUser>>() {
            @Override
            public void onResponse(@NonNull Call<List<ModelUser>> call, @NonNull Response<List<ModelUser>> response) {
                modelUsers = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelUsers).size(); i++) {
                    listAuto.add(modelUsers.get(i).getColName());
                    mToolbar.setTitle("Halo, " + modelUsers.get(i).getColName());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<ModelUser>> call, Throwable t) {
                Log.d("response", Arrays.toString(t.getStackTrace()));
                progressDialog.dismiss();
            }
        });

        //Call Total Cash
        Call<ArrayList<ModelTotalCash>> callTotalCash = apiInterface.view_total_cash(sharedPrefManager.getSPEmail());
        callTotalCash.enqueue(new Callback<ArrayList<ModelTotalCash>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelTotalCash>> call, Response<ArrayList<ModelTotalCash>> response) {
                modelTotalCashes = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelTotalCashes).size(); i++) {
                    listAuto.add(modelTotalCashes.get(i).getColUserEmail());
                    String sTotalCash = modelTotalCashes.get(i).getAmountTotal();

                    textBalance.setText(formatRupiah(Double.parseDouble(sTotalCash)));
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<ModelTotalCash>> call, Throwable t) {
                Log.d("response", Arrays.toString(t.getStackTrace()));
                progressDialog.dismiss();
            }
        });

        //Call Total Bank
        Call<ArrayList<ModelTotalBank>> callTotalBank = apiInterface.view_total_bank(sharedPrefManager.getSPEmail());
        callTotalBank.enqueue(new Callback<ArrayList<ModelTotalBank>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelTotalBank>> call, Response<ArrayList<ModelTotalBank>> response) {
                modelTotalBanks = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelTotalBanks).size(); i++) {
                    listAuto.add(modelTotalBanks.get(i).getColUserEmail());
                    String sTotalCash = modelTotalBanks.get(i).getAmountTotal();

                    textAmountBank.setText(formatRupiah(Double.parseDouble(sTotalCash)));
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<ModelTotalBank>> call, Throwable t) {
                Log.d("response", Arrays.toString(t.getStackTrace()));
                progressDialog.dismiss();
            }
        });

        //Call Total Wallet
        Call<ArrayList<ModelTotalWallet>> callTotalWallet = apiInterface.view_total_wallet(sharedPrefManager.getSPEmail());
        callTotalWallet.enqueue(new Callback<ArrayList<ModelTotalWallet>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelTotalWallet>> call, Response<ArrayList<ModelTotalWallet>> response) {
                modelTotalWallets = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelTotalWallets).size(); i++) {
                    listAuto.add(modelTotalWallets.get(i).getColUserEmail());
                    String sTotalCash = modelTotalWallets.get(i).getAmountTotal();

                    textEwallet.setText(formatRupiah(Double.parseDouble(sTotalCash)));
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<ModelTotalWallet>> call, Throwable t) {
                Log.d("response", Arrays.toString(t.getStackTrace()));
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

//        final MenuItem menu_notif = menu.findItem(R.id.menu_notif);
//        View actionView = MenuItemCompat.getActionView(menu_notif);
//        notif_badge = actionView.findViewById(R.id.notif_badge);
//        setupBadge();
//        actionView.setOnClickListener(v -> onOptionsItemSelected(menu_notif));

        return true;
    }

//    private void setupBadge() {
//        if (notif_badge == null) return;
//        if (notification_count == 0) {
//            notif_badge.setVisibility(View.GONE);
//        } else {
//            notif_badge.setVisibility(View.VISIBLE);
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == R.id.menu_notif) {
            Toast.makeText(this, "Lanjutkan ini", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }

}