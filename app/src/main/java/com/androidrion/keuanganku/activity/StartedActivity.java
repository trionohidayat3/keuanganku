/*
 * Keuanganku
 * StartedActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/17/22, 11:55 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.card.MaterialCardView;
import com.onesignal.OneSignal;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;

public class StartedActivity extends AppCompatActivity {

    private static final String ONESIGNAL_APP_ID = "########-####-####-####-############";

    MaterialCardView cardDaftar, cardMasuk;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_started);

        sharedPrefManager = new SharedPrefManager(this);

        if (sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(this, MainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);

        cardDaftar = findViewById(R.id.card_daftar);
        cardMasuk = findViewById(R.id.card_masuk);

        cardDaftar.setOnClickListener(view -> startActivity(new Intent(this, RegisterActivity.class)));
        cardMasuk.setOnClickListener(view -> startActivity(new Intent(this, LoginActivity.class)));
    }
}