/*
 * Keuanganku
 * SettingActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/17/22, 1:52 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.BuildConfig;
import com.androidrion.keuanganku.ModelUser;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.Server;
import com.androidrion.keuanganku.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;
    CollapsingToolbarLayout collapsingToolbar;
    Toolbar toolbar;
    ImageView imageInfoUser;
    TextView textInfoPhone, textInfoEmail, textInfoAddress, textVersion;
    FloatingActionButton fabEditProfile;
    LinearLayout settingKeluar;

    API_Interface apiInterface;
    List<ModelUser> modelUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        sharedPrefManager = new SharedPrefManager(this);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imageInfoUser = findViewById(R.id.image_user);

        textInfoPhone = findViewById(R.id.text_info_phone);
        textInfoEmail = findViewById(R.id.text_info_email);
        textInfoAddress = findViewById(R.id.text_info_address);

        apiInterface = API_Client.getClient().create(API_Interface.class);
        Call<List<ModelUser>> listCall = apiInterface.user_view(sharedPrefManager.getSPEmail());
        listCall.enqueue(new Callback<List<ModelUser>>() {
            @Override
            public void onResponse(@NonNull Call<List<ModelUser>> call, @NonNull Response<List<ModelUser>> response) {
                modelUsers = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelUsers).size(); i++) {
                    listAuto.add(modelUsers.get(i).getColName());
                    collapsingToolbar.setTitle(modelUsers.get(i).getColName());
                    Picasso.with(SettingActivity.this).load(Server.URL + modelUsers.get(i).getColPhotoProfile()).into(imageInfoUser);
                    textInfoPhone.setText(modelUsers.get(i).getColPhoneNumber());
                    textInfoEmail.setText(modelUsers.get(i).getColEmail());
                    textInfoAddress.setText(modelUsers.get(i).getColAddress());
                }
            }

            @Override
            public void onFailure(Call<List<ModelUser>> call, Throwable t) {

            }
        });

        fabEditProfile = findViewById(R.id.fab_edit_profile);
        fabEditProfile.setOnClickListener(view -> startActivity(new Intent(this, EditProfileActivity.class)));

        settingKeluar = findViewById(R.id.setting_keluar);
        settingKeluar.setOnClickListener(view -> {
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            startActivity(new Intent(this, StartedActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        });

        textVersion = findViewById(R.id.text_version);
        textVersion.setText("Versi " + BuildConfig.VERSION_NAME);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}