/*
 * Keuanganku
 * HistoryActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/13/22, 3:48 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelTransaction;
import com.androidrion.keuanganku.ModelTypeTrans;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;
import com.androidrion.keuanganku.TransactionAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat, dateFormatDB;
    Calendar calendarFirst, calendarNow;

    TextInputEditText textDateStart, textDateEnd;
    AutoCompleteTextView autoTypeTransaction;
    Button btnFilter;

    String sNewDateFirst, sNewDateEnd;

    ProgressDialog progressDialog;

    RecyclerView rvHistory;
    API_Interface apiInterface;

    TransactionAdapter transactionAdapter;

    List<ModelTransaction> modelTransactionList;

    List<ModelTypeTrans> modelTypeTrans;
    ArrayAdapter<String> adapter;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Riwayat");

        sharedPrefManager = new SharedPrefManager(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormatDB = new SimpleDateFormat("yyyy-MM-dd");

        calendarFirst = Calendar.getInstance();
        calendarFirst.set(Calendar.DAY_OF_MONTH, 1);

        calendarNow = Calendar.getInstance();

        rvHistory = findViewById(R.id.rv_history);
        rvHistory.setHasFixedSize(true);
        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(linearLayoutManager);


        apiInterface = API_Client.getClient().create(API_Interface.class);

        Call<List<ModelTransaction>> callTransaction = apiInterface.view_transaction(sharedPrefManager.getSPEmail());
        callTransaction.enqueue(new Callback<List<ModelTransaction>>() {
            @Override
            public void onResponse(Call<List<ModelTransaction>> call, Response<List<ModelTransaction>> response) {
                modelTransactionList = response.body();
                transactionAdapter = new TransactionAdapter(modelTransactionList);
                rvHistory.setAdapter(transactionAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelTransaction>> call, Throwable t) {

            }
        });

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_filter) {
            showDialogFilter();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogFilter() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_filter, null);

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        autoTypeTransaction = view.findViewById(R.id.auto_type_transaction);
        textDateStart = view.findViewById(R.id.date_start);
        textDateEnd = view.findViewById(R.id.date_end);
        btnFilter = view.findViewById(R.id.btn_filter);

        autoTypeTransaction.setOnClickListener(viewTypeTransaction -> {

            progressDialog.show();
            Call<List<ModelTypeTrans>> callTypeTrans = apiInterface.view_type_trans();
            callTypeTrans.enqueue(new Callback<List<ModelTypeTrans>>() {
                @Override
                public void onResponse(Call<List<ModelTypeTrans>> call, Response<List<ModelTypeTrans>> response) {
                    modelTypeTrans = response.body();
                    List<String> listAuto = new ArrayList<>();
                    for (int i = 0; i < Objects.requireNonNull(modelTypeTrans).size(); i++) {
                        listAuto.add(modelTypeTrans.get(i).getColType());
                    }

                    adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                    autoTypeTransaction.setAdapter(adapter);
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<ModelTypeTrans>> call, Throwable t) {
                    Log.d("response", Arrays.toString(t.getStackTrace()));
                    progressDialog.dismiss();
                }
            });
        });

        textDateStart.setOnClickListener(view1 -> dialogDateStart());
        textDateEnd.setOnClickListener(view1 -> dialogDateEnd());

        btnFilter.setOnClickListener(view1 -> {
            String sTypeTransaction = autoTypeTransaction.getText().toString();

            Call<List<ModelTransaction>> callFilterTransaction;
            if (sTypeTransaction.equals("SEMUA")) {
                callFilterTransaction = apiInterface.view_history_filter(sharedPrefManager.getSPEmail(), "", sNewDateFirst, sNewDateEnd);
            } else {
                callFilterTransaction = apiInterface.view_history_filter(sharedPrefManager.getSPEmail(), sTypeTransaction, sNewDateFirst, sNewDateEnd);
            }
            callFilterTransaction.enqueue(new Callback<List<ModelTransaction>>() {
                @Override
                public void onResponse(Call<List<ModelTransaction>> call, Response<List<ModelTransaction>> response) {
                    modelTransactionList = response.body();
                    transactionAdapter = new TransactionAdapter(modelTransactionList);
                    rvHistory.setAdapter(transactionAdapter);
                }

                @Override
                public void onFailure(Call<List<ModelTransaction>> call, Throwable t) {

                }
            });

            mBottomSheetDialog.hide();
        });

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void dialogDateStart() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            textDateStart.setText(dateFormat.format(newDate.getTime()));

            sNewDateFirst = dateFormatDB.format(newDate.getTime());

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void dialogDateEnd() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            textDateEnd.setText(dateFormat.format(newDate.getTime()));

            sNewDateEnd = dateFormatDB.format(newDate.getTime());

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}