/*
 * Keuanganku
 * RequestActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/25/22, 4:23 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelRequest;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.RequestAdapter;
import com.androidrion.keuanganku.SharedPrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    RecyclerView recycler_request;

    API_Interface apiInterface;

    RequestAdapter requestAdapter;
    ArrayList<ModelRequest> modelRequestArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Transaksi E-wallet");

        sharedPrefManager = new SharedPrefManager(this);

        recycler_request = findViewById(R.id.recycler_request);
        recycler_request.setHasFixedSize(true);
        recycler_request.setLayoutManager(new LinearLayoutManager(this));

        apiInterface = API_Client.getClient().create(API_Interface.class);

        Call<ArrayList<ModelRequest>> callRequest = apiInterface.view_request(sharedPrefManager.getSPEmail());
        callRequest.enqueue(new Callback<ArrayList<ModelRequest>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelRequest>> call, Response<ArrayList<ModelRequest>> response) {
                modelRequestArrayList = response.body();
                requestAdapter = new RequestAdapter(modelRequestArrayList, getApplication());
                recycler_request.setAdapter(requestAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<ModelRequest>> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}