/*
 * Keuanganku
 * PayActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/17/22, 2:17 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelCard;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

//    ImageView imgPicture;

    //    Button btnOpen, btnUpload;
    RadioGroup groupTo;
    RadioButton radioKas, radioBank;
    TextInputLayout layoutBank;
    AutoCompleteTextView autoBankAccount;
    TextInputEditText inputDate, inputAmount, inputNote;
    Button btnUpload;

    String sRadio;

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormatText, dateFormat, dateFormatPayCode;
    Calendar calendar;

//    Bitmap bitmap;

    API_Interface apiInterface;
    List<ModelCard> modelCards;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        sharedPrefManager = new SharedPrefManager(this);

        apiInterface = API_Client.getClient().create(API_Interface.class);

        calendar = Calendar.getInstance();
        dateFormatText = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateFormatPayCode = new SimpleDateFormat("ddmmyyhhmmss");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Pengeluaran");

//        imgPicture = findViewById(R.id.image_picture);
//        btnOpen = findViewById(R.id.btn_open_camera);
        groupTo = findViewById(R.id.group_to);
        radioKas = findViewById(R.id.radio_kas);
        radioBank = findViewById(R.id.radio_bank);
        layoutBank = findViewById(R.id.layout_bank);
        autoBankAccount = findViewById(R.id.auto_bank_account);
        inputDate = findViewById(R.id.txt_date);
        inputAmount = findViewById(R.id.txt_amount);
        inputNote = findViewById(R.id.txt_note);
        btnUpload = findViewById(R.id.btn_pay);

//        groupTo.setOnCheckedChangeListener((radioGroup, i) -> {
//            int selectedId = groupTo.getCheckedRadioButtonId();
//
//            if (selectedId == radioKas.getId()) {
//                sRadio = "Kas";
//            } else if (selectedId == radioBank.getId()) {
//                sRadio = "Bank";
//            } else {
//                sRadio = "Tidak ada";
//            }
//        });

//        btnOpen.setOnClickListener(view -> getImage());
        groupTo.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = groupTo.getCheckedRadioButtonId();

            if (selectedId == radioKas.getId()) {
                layoutBank.setVisibility(View.GONE);
            } else if (selectedId == radioBank.getId()) {
                layoutBank.setVisibility(View.VISIBLE);
            } else {
                layoutBank.setVisibility(View.GONE);
            }
        });

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccount.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        inputDate.setOnClickListener(view -> showDate());
        btnUpload.setOnClickListener(view -> uploadPay());
    }

    private void showDate() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDate.setText(dateFormatText.format(newDate.getTime()));

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void uploadPay() {
        String amount = inputAmount.getText().toString();
        String bankName = autoBankAccount.getText().toString();
        String sNote = inputNote.getText().toString();
        String date_time = dateFormat.format(calendar.getTime());

        if (radioKas.isChecked()) {
            Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("out", amount, "Kas", sNote, date_time, sharedPrefManager.getSPEmail());
            callTransactionKas.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    backToMain();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } else if (radioBank.isChecked()) {
            Call<ResponseBody> callTransactionBank = apiInterface.add_transaction_bank("out", amount, "Bank", bankName, sNote, date_time, sharedPrefManager.getSPEmail());
            callTransactionBank.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    backToMain();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }


    }

    private void backToMain() {
        startActivity(new Intent(PayActivity.this, MainActivity.class));
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}