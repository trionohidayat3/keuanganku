/*
 * Keuanganku
 * EditProfileActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/11/22, 9:26 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelUser;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.Server;
import com.androidrion.keuanganku.SharedPrefManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;
    ImageView ivProfile;
    ExtendedFloatingActionButton fabChangePhoto;
    TextInputEditText edtName, edtEmail, edtPassword, edtPhone, edtAddress;

    Bitmap bitmap;


    API_Interface apiInterface;
    List<ModelUser> modelUsers;

    SimpleDateFormat dateFormat;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        sharedPrefManager = new SharedPrefManager(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Edit Profile");

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        ivProfile = findViewById(R.id.image_profile);
        fabChangePhoto = findViewById(R.id.fab_change_photo);
        fabChangePhoto.setOnClickListener(view -> getImage());
        edtName = findViewById(R.id.edit_text_name);
        edtEmail = findViewById(R.id.edit_text_email);
        edtPassword = findViewById(R.id.edit_text_password);
        edtPhone = findViewById(R.id.edit_text_phone);
        edtAddress = findViewById(R.id.edit_text_address);

        apiInterface = API_Client.getClient().create(API_Interface.class);
        Call<List<ModelUser>> listCall = apiInterface.user_view(sharedPrefManager.getSPEmail());
        listCall.enqueue(new Callback<List<ModelUser>>() {
            @Override
            public void onResponse(@NonNull Call<List<ModelUser>> call, @NonNull Response<List<ModelUser>> response) {
                modelUsers = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelUsers).size(); i++) {
                    listAuto.add(modelUsers.get(i).getColName());
                    Picasso.with(EditProfileActivity.this).load(Server.URL + modelUsers.get(i).getColPhotoProfile()).into(ivProfile);
                    edtName.setText(modelUsers.get(i).getColName());
                    edtPhone.setText(modelUsers.get(i).getColPhoneNumber());
                    edtEmail.setText(modelUsers.get(i).getColEmail());
                    edtAddress.setText(modelUsers.get(i).getColAddress());
                }
            }

            @Override
            public void onFailure(Call<List<ModelUser>> call, Throwable t) {

            }
        });
    }

    private void getImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.menu_save) {

            String newName = edtName.getText().toString();
            String newEmail = edtEmail.getText().toString();
            String newPassword = edtPassword.getText().toString();
            String newPhone = edtPhone.getText().toString();
            String newAddress = edtAddress.getText().toString();
            String imgdata = imgToString();
            String date_time = dateFormat.format(calendar.getTime());

            Call<ResponseBody> listCall = apiInterface.user_update(newName, newEmail, newPassword, newPhone, newAddress, imgdata, date_time);
            listCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (response.isSuccessful()) {

                        Intent intent = new Intent(EditProfileActivity.this, SettingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

        return super.onOptionsItemSelected(item);
    }


    private String imgToString() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgByte, Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {
        super.onActivityResult(RC, RQC, I);
        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {
            Uri uri = I.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                ivProfile.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}