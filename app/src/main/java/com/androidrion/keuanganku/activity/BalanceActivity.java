/*
 * Keuanganku
 * BalanceActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/13/22, 4:36 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelTotalCash;
import com.androidrion.keuanganku.ModelTotalWallet;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalanceActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    SwipeRefreshLayout swipeBalance;
    TextView textWallet;

    MaterialButton btnScan, btnTransfer, btnTopupWallet, btnTarikWallet;

    SimpleDateFormat dateFormat;
    Calendar calendar;

    API_Interface apiInterface;


    ProgressDialog progressDialog;


    ArrayList<ModelTotalCash> modelTotalCashes;
    ArrayList<ModelTotalWallet> modelTotalWallets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Dompet");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        progressDialog.show();

        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap("content", BarcodeFormat.QR_CODE, 700, 700);
            ImageView imageViewQrCode = findViewById(R.id.image_qrcode);
            imageViewQrCode.setImageBitmap(bitmap);
        } catch(Exception ignored) {

        }

        apiInterface = API_Client.getClient().create(API_Interface.class);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        sharedPrefManager = new SharedPrefManager(this);

        swipeBalance = findViewById(R.id.swipe_balance);
        swipeBalance.setOnRefreshListener(() -> {
            swipeBalance.setRefreshing(false);
            loadData();
        });

        textWallet = findViewById(R.id.text_ewallet);

        String rupiah = "0";

        textWallet.setText(formatRupiah(Double.parseDouble(rupiah)));

        btnScan = findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(view -> scanQRCode());
        loadData();

    }

    private void scanQRCode() {
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setPrompt("Scan a barcode or QR Code");
        intentIntegrator.setOrientationLocked(true);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        // if the intentResult is null then
        // toast a message as "cancelled"
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                // if the intentResult is not null we'll set
                // the content and format of scan message
                Toast.makeText(this, intentResult.getContents(), Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadData() {
        Call<ArrayList<ModelTotalWallet>> callTotalWallet = apiInterface.view_total_wallet(sharedPrefManager.getSPEmail());
        callTotalWallet.enqueue(new Callback<ArrayList<ModelTotalWallet>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelTotalWallet>> call, Response<ArrayList<ModelTotalWallet>> response) {
                modelTotalWallets = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelTotalWallets).size(); i++) {
                    listAuto.add(modelTotalWallets.get(i).getColUserEmail());
                    String sTotalCash = modelTotalWallets.get(i).getAmountTotal();

                    textWallet.setText(formatRupiah(Double.parseDouble(sTotalCash)));
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<ModelTotalWallet>> call, Throwable t) {
                Log.d("response", Arrays.toString(t.getStackTrace()));
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}