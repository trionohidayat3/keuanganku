/*
 * Keuanganku
 * AccountBankActivity.java
 *
 * Created by Triono Hidayat on 5/19/22, 2:28 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/19/22, 2:28 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.CardAdapter;
import com.androidrion.keuanganku.ModelBank;
import com.androidrion.keuanganku.ModelCard;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountBankActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    RecyclerView recyclerBankAccount;
    ExtendedFloatingActionButton fabAddAccountBank;

    String sBank, sNumber, sName;

    CardAdapter cardAdapter;

    API_Interface apiInterface;

    ArrayAdapter<String> adapter;
    List<ModelCard> modelCards;
    List<ModelBank> modelBanks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_bank);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Daftar Rekening Bank");

        sharedPrefManager = new SharedPrefManager(this);

        apiInterface = API_Client.getClient().create(API_Interface.class);

        recyclerBankAccount = findViewById(R.id.recycler_bank_account);
        recyclerBankAccount.setHasFixedSize(true);
        recyclerBankAccount.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerBankAccount.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                if (response.isSuccessful()) {
                    modelCards = response.body();
                    cardAdapter = new CardAdapter(modelCards);
                    recyclerBankAccount.setAdapter(cardAdapter);
                } else {
                    Toast.makeText(AccountBankActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        fabAddAccountBank = findViewById(R.id.fab_add_account_bank);
        fabAddAccountBank.setOnClickListener(view -> openDialogAddBank());
    }

    private void openDialogAddBank() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_card);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ImageView imageBankLogo;
        AutoCompleteTextView autoBankName;
        TextInputEditText inputNoAccount, inputNameAccount;

        imageBankLogo = dialog.findViewById(R.id.img_logo_bank);
        autoBankName = dialog.findViewById(R.id.auto_bank_name);
        inputNoAccount = dialog.findViewById(R.id.txt_no_rek);
        inputNameAccount = dialog.findViewById(R.id.txt_nama_rek);

        inputNameAccount.setText(sharedPrefManager.getSpName());

        Call<List<ModelBank>> callBank = apiInterface.view_bank();
        callBank.enqueue(new Callback<List<ModelBank>>() {
            @Override
            public void onResponse(Call<List<ModelBank>> call, Response<List<ModelBank>> response) {
                modelBanks = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelBanks).size(); i++) {
                    listAuto.add(modelBanks.get(i).getColNameBank());

                }

                adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankName.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ModelBank>> call, Throwable t) {

            }
        });

        dialog.findViewById(R.id.bt_save).setOnClickListener(v -> {
            sBank = autoBankName.getText().toString();
            sNumber = inputNoAccount.getText().toString();
            sName = inputNameAccount.getText().toString();
            if (sBank.isEmpty()) {
                Toast.makeText(AccountBankActivity.this, "Harap pilih bank Anda", Toast.LENGTH_SHORT).show();
            } else if (sNumber.isEmpty()) {
                Toast.makeText(AccountBankActivity.this, "Harap isi nomor rekening Anda", Toast.LENGTH_SHORT).show();
            } else if (sName.isEmpty()) {
                Toast.makeText(AccountBankActivity.this, "Harap isi nama pemilik rekening", Toast.LENGTH_SHORT).show();
            } else {
                Call<ResponseBody> callAddBank = apiInterface.add_card(sBank, sNumber, sName, sharedPrefManager.getSPEmail());
                callAddBank.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                dialog.dismiss();
            }


        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}