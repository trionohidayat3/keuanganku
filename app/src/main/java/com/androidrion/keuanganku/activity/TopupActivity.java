/*
 * Keuanganku
 * TopupActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 5:30 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.androidrion.keuanganku.R;

public class TopupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Top Up");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}