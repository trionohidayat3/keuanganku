/*
 * Keuanganku
 * RegisterActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/11/22, 4:46 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    TextInputEditText inputFullName, inputEmail, inputPassword, inputConfPassword;
    MaterialCardView cardDaftar;

    String sName, sEmail, sPassword, sConfPassword;

    API_Interface apiInterface;

    SimpleDateFormat dateFormat;
    Calendar calendar;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Daftar");

        sharedPrefManager = new SharedPrefManager(this);

        apiInterface = API_Client.getClient().create(API_Interface.class);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        inputFullName = findViewById(R.id.input_full_name);
        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);
        inputConfPassword = findViewById(R.id.input_config_password);

        cardDaftar = findViewById(R.id.card_daftar);
        cardDaftar.setOnClickListener(view -> {
            sName = inputFullName.getText().toString();
            sEmail = inputEmail.getText().toString();
            sPassword = inputPassword.getText().toString();
            sConfPassword = inputConfPassword.getText().toString();
            String date_time = dateFormat.format(calendar.getTime());

            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAME, sName);
            if (sName.isEmpty() || sEmail.isEmpty() || sPassword.isEmpty() || sConfPassword.isEmpty()) {
                Toast.makeText(this, "Mohon isi semua form", Toast.LENGTH_SHORT).show();
            } else if (!sPassword.equals(sConfPassword)) {
                Toast.makeText(this, "Password Tidak Cocok!", Toast.LENGTH_SHORT).show();
            } else if (sPassword.length() < 7) {
                Toast.makeText(this, "Masukkan 8 karakter untuk kata sandi", Toast.LENGTH_SHORT).show();
            } else {
                Call<ResponseBody> callRegister = apiInterface.create_user(sName, sEmail, sPassword, "-", "-", "-", date_time, "-");
                callRegister.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        showDialogSuccess();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                Call<ResponseBody> call = apiInterface.add_cash("0", "in", date_time, sEmail);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

//                Call<ResponseBody> addCustomer = apiInterface.add_customer(sName, sEmail, "-", "-", sharedPrefManager.getSPEmail());
//                addCustomer.enqueue(new Callback<ResponseBody>() {
//                    @Override
//                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                    }
//                });
            }
        });

    }

    private void showDialogSuccess() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_success_register);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        dialog.findViewById(R.id.card_mulai).setOnClickListener(v -> {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            intent.putExtra("INTENT_NAME", sName);
            startActivity(intent);
            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}