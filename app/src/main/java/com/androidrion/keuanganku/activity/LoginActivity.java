/*
 * Keuanganku
 * LoginActivity.java
 *
 * Created by Triono Hidayat on 5/17/22, 3:44 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/11/22, 4:39 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.androidrion.keuanganku.API_Client;
import com.androidrion.keuanganku.API_Interface;
import com.androidrion.keuanganku.ModelUser;
import com.androidrion.keuanganku.R;
import com.androidrion.keuanganku.SharedPrefManager;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText inputEmail, inputPassword;
    MaterialCardView cardLogin;

    String email, password;

    API_Interface apiInterface;

    SharedPrefManager sharedPrefManager;

    ModelUser modelUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Masuk");

        apiInterface = API_Client.getClient().create(API_Interface.class);

        sharedPrefManager = new SharedPrefManager(this);

        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);
        cardLogin = findViewById(R.id.card_login);

        modelUser = new ModelUser();
        modelUser.setColEmail(inputEmail.getText().toString());

        cardLogin.setOnClickListener(view -> {

            progressDialog.show();
            email = inputEmail.getText().toString();
            password = inputPassword.getText().toString();

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Mohon isi semua form", Toast.LENGTH_SHORT).show();
            } else {
                Call<List<ModelUser>> call = apiInterface.user_login(email, password);
                call.enqueue(new Callback<List<ModelUser>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<ModelUser>> call, @NonNull Response<List<ModelUser>> response) {
                        if (response.body().isEmpty()) {
                            Toast.makeText(LoginActivity.this, "Maaf, coba lagi", Toast.LENGTH_SHORT).show();
                        } else {
                            sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, email);
                            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                            progressDialog.dismiss();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ModelUser>> call, Throwable t) {
                        Log.d("response", Arrays.toString(t.getStackTrace()));
                        progressDialog.dismiss();
                    }
                });
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}