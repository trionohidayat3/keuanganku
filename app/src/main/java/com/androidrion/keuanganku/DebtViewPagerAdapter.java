/*
 * Keuanganku
 * DebtViewPagerAdapter.java
 *
 * Created by Triono Hidayat on 6/9/22, 1:19 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/9/22, 1:19 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class DebtViewPagerAdapter extends FragmentPagerAdapter {

    public DebtViewPagerAdapter(
            @NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
            fragment = new DeptMeFragment();
        else if (position == 1)
            fragment = new DeptCustomerFragment();

        assert fragment != null;
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
            title = "Saya";
        else if (position == 1)
            title = "Pelanggan";

        return title;
    }
}