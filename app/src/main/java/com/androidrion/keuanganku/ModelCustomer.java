/*
 * Keuanganku
 * ModelCustomer.java
 *
 * Created by Triono Hidayat on 5/25/22, 8:53 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/25/22, 8:53 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelCustomer {

    @SerializedName("col_id")
    @Expose
    private String colId;
    @SerializedName("col_customer_email")
    @Expose
    private String colCustomerEmail;
    @SerializedName("col_customer_name")
    @Expose
    private String colCustomerName;
    @SerializedName("col_user_email")
    @Expose
    private String colUserEmail;
    @SerializedName("col_number_phone")
    @Expose
    private String colNumberPhone;
    @SerializedName("col_address")
    @Expose
    private String colAddress;

    /**
     * No args constructor for use in serialization
     *
     */
    public ModelCustomer() {
    }

    /**
     *
     * @param colCustomerName
     * @param colUserEmail
     * @param colAddress
     * @param colNumberPhone
     * @param colCustomerEmail
     * @param colId
     */
    public ModelCustomer(String colId, String colCustomerEmail, String colCustomerName, String colUserEmail, String colNumberPhone, String colAddress) {
        super();
        this.colId = colId;
        this.colCustomerEmail = colCustomerEmail;
        this.colCustomerName = colCustomerName;
        this.colUserEmail = colUserEmail;
        this.colNumberPhone = colNumberPhone;
        this.colAddress = colAddress;
    }

    public String getColId() {
        return colId;
    }

    public void setColId(String colId) {
        this.colId = colId;
    }

    public String getColCustomerEmail() {
        return colCustomerEmail;
    }

    public void setColCustomerEmail(String colCustomerEmail) {
        this.colCustomerEmail = colCustomerEmail;
    }

    public String getColCustomerName() {
        return colCustomerName;
    }

    public void setColCustomerName(String colCustomerName) {
        this.colCustomerName = colCustomerName;
    }

    public String getColUserEmail() {
        return colUserEmail;
    }

    public void setColUserEmail(String colUserEmail) {
        this.colUserEmail = colUserEmail;
    }

    public String getColNumberPhone() {
        return colNumberPhone;
    }

    public void setColNumberPhone(String colNumberPhone) {
        this.colNumberPhone = colNumberPhone;
    }

    public String getColAddress() {
        return colAddress;
    }

    public void setColAddress(String colAddress) {
        this.colAddress = colAddress;
    }

}
