/*
 * Keuanganku
 * ReportActivity.java
 *
 * Created by Triono Hidayat on 5/20/22, 11:18 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/20/22, 11:18 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.Calendar;

public class ReportActivity extends AppCompatActivity {

    AutoCompleteTextView autoMonthYear;
    Calendar calendar;

    ViewPager viewPager;
    TabLayout tabLayout;
    ReportViewPagerAdapter reportViewPagerAdapter;
    private int currentYear;
    private int yearSelected;
    private int monthSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Laporan");

        calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        yearSelected = currentYear;
        monthSelected = calendar.get(Calendar.MONTH);

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.view_pager);

        reportViewPagerAdapter = new ReportViewPagerAdapter(this);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setAdapter(reportViewPagerAdapter);

//        tabLayout.addTab(tabLayout.newTab().setText("Pemasukan"));
//        tabLayout.addTab(tabLayout.newTab().setText("Pengeluaran"));

        autoMonthYear = findViewById(R.id.auto_month_year);
        autoMonthYear.setOnClickListener(view -> {
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}