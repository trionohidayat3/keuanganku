/*
 * Keuanganku
 * ModelTransactionDebt.java
 *
 * Created by Triono Hidayat on 5/24/22, 8:50 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/24/22, 8:50 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTransactionDebt {

    @SerializedName("col_id")
    @Expose
    private String colId;
    @SerializedName("col_type")
    @Expose
    private String colType;
    @SerializedName("col_type_trans")
    @Expose
    private String colTypeTrans;
    @SerializedName("col_person")
    @Expose
    private String colPerson;
    @SerializedName("col_amount")
    @Expose
    private String colAmount;
    @SerializedName("col_date")
    @Expose
    private String colDate;
    @SerializedName("col_date_due")
    @Expose
    private String colDateDue;
    @SerializedName("col_note")
    @Expose
    private String colNote;
    @SerializedName("col_status")
    @Expose
    private String colStatus;
    @SerializedName("col_ref")
    @Expose
    private String colRef;

    @SerializedName("col_user_email")
    @Expose
    private String colUserEmail;

    @SerializedName("col_create")
    @Expose
    private String colCreate;

    @SerializedName("col_total_amount")
    @Expose
    private String colTotalAmount;

    @SerializedName("col_total_amount_fix")
    @Expose
    private String colTotalAmountFix;

    @SerializedName("col_debt_note")
    @Expose
    private String colDebtNote;

    @SerializedName("total_amount")
    @Expose
    private String totalAmount;

    @SerializedName("col_debt_type")
    @Expose
    private String colDebtType;

    /**
     * No args constructor for use in serialization
     */
    public ModelTransactionDebt() {
    }

    /**
     * @param colAmount
     * @param colUserEmail
     * @param colType
     * @param colPerson
     * @param colDateDue
     * @param colCreate
     * @param colDate
     * @param colNote
     * @param colId
     * @param colTypeTrans
     * @param colRef
     * @param colStatus
     */
    public ModelTransactionDebt(String colId, String colType, String colTypeTrans, String colPerson, String colAmount, String colDate, String colDateDue, String colNote, String colStatus, String colRef, String colUserEmail, String colCreate, String colTotalAmount, String colTotalAmountFix, String colDebtNote, String totalAmount, String colDebtType) {
        super();
        this.colId = colId;
        this.colType = colType;
        this.colTypeTrans = colTypeTrans;
        this.colPerson = colPerson;
        this.colAmount = colAmount;
        this.colDate = colDate;
        this.colDateDue = colDateDue;
        this.colNote = colNote;
        this.colStatus = colStatus;
        this.colRef = colRef;
        this.colUserEmail = colUserEmail;
        this.colCreate = colCreate;
        this.colTotalAmount = colTotalAmount;
        this.colTotalAmountFix = colTotalAmountFix;
        this.colDebtNote = colDebtNote;
        this.totalAmount = totalAmount;
        this.colDebtType = colDebtType;
    }

    public String getColId() {
        return colId;
    }

    public void setColId(String colId) {
        this.colId = colId;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColTypeTrans() {
        return colTypeTrans;
    }

    public void setColTypeTrans(String colTypeTrans) {
        this.colTypeTrans = colTypeTrans;
    }

    public String getColPerson() {
        return colPerson;
    }

    public void setColPerson(String colPerson) {
        this.colPerson = colPerson;
    }

    public String getColAmount() {
        return colAmount;
    }

    public void setColAmount(String colAmount) {
        this.colAmount = colAmount;
    }

    public String getColDate() {
        return colDate;
    }

    public void setColDate(String colDate) {
        this.colDate = colDate;
    }

    public String getColDateDue() {
        return colDateDue;
    }

    public void setColDateDue(String colDateDue) {
        this.colDateDue = colDateDue;
    }

    public String getColNote() {
        return colNote;
    }

    public void setColNote(String colNote) {
        this.colNote = colNote;
    }

    public String getColStatus() {
        return colStatus;
    }

    public void setColStatus(String colStatus) {
        this.colStatus = colStatus;
    }

    public String getColRef() {
        return colRef;
    }

    public void setColRef(String colRef) {
        this.colRef = colRef;
    }

    public String getColUserEmail() {
        return colUserEmail;
    }

    public void setColUserEmail(String colUserEmail) {
        this.colUserEmail = colUserEmail;
    }

    public String getColCreate() {
        return colCreate;
    }

    public void setColCreate(String colCreate) {
        this.colCreate = colCreate;
    }

    public String getColTotalAmount() {
        return colTotalAmount;
    }

    public void setColTotalAmount(String colTotalAmount) {
        this.colTotalAmount = colTotalAmount;
    }

    public String getColTotalAmountFix() {
        return colTotalAmountFix;
    }

    public void setColTotalAmountFix(String colTotalAmountFix) {
        this.colTotalAmountFix = colTotalAmountFix;
    }

    public String getColDebtNote() {
        return colDebtNote;
    }

    public void setColDebtNote(String colDebtNote) {
        this.colDebtNote = colDebtNote;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getColDebtType() {
        return colDebtType;
    }

    public void setColDebtType(String colDebtType) {
        this.colDebtType = colDebtType;
    }
}
