/*
 * Keuanganku
 * API_Interface.java
 *
 * Created by Triono Hidayat on 4/20/22, 1:34 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 12:55 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API_Interface {

    @FormUrlEncoded
    @POST("user_register.php")
    Call<ResponseBody> create_user(@Field("col_name") String col_name,
                                   @Field("col_email") String col_email,
                                   @Field("col_password") String col_password,
                                   @Field("col_phone_number") String col_phone_number,
                                   @Field("col_address") String col_address,
                                   @Field("col_photo_profile") String col_photo_profile,
                                   @Field("col_date_create") String col_date_create,
                                   @Field("col_date_update") String col_date_update);

    @GET("user_login.php")
    Call<List<ModelUser>> user_login(@Query("col_email") String col_email,
                                     @Query("col_password") String col_password);

    @GET("user_view.php")
    Call<List<ModelUser>> user_view(@Query("col_email") String col_email);

    @FormUrlEncoded
    @POST("user_update.php")
    Call<ResponseBody> user_update(@Field("col_name") String col_name,
                                   @Field("col_email") String col_email,
                                   @Field("col_password") String col_password,
                                   @Field("col_phone_number") String col_phone_number,
                                   @Field("col_address") String col_address,
                                   @Field("col_photo_profile") String col_photo_profile,
                                   @Field("col_date_update") String col_date_update);

    @GET("view_bank.php")
    Call<List<ModelBank>> view_bank();

    @FormUrlEncoded
    @POST("add_card.php")
    Call<ResponseBody> add_card(@Field("col_name_bank") String col_name_bank,
                                @Field("col_account_number") String col_account_number,
                                @Field("col_name_owner") String col_name_owner,
                                @Field("col_user_email") String col_user_email);

    @GET("view_card.php")
    Call<List<ModelCard>> view_card(@Query("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_cash.php")
    Call<ResponseBody> add_cash(@Field("col_amount") String col_amount,
                                @Field("col_type_transc") String col_type_transc,
                                @Field("col_date") String col_date,
                                @Field("col_user_email") String col_user_email);

    @GET("view_total_cash.php")
    Call<ArrayList<ModelTotalCash>> view_total_cash(@Query("col_user_email") String col_user_email);

    @GET("view_total_bank.php")
    Call<ArrayList<ModelTotalBank>> view_total_bank(@Query("col_user_email") String col_user_email);

    @GET("view_total_wallet.php")
    Call<ArrayList<ModelTotalWallet>> view_total_wallet(@Query("col_user_email") String col_user_email);

    @GET("view_total_bank_specific.php")
    Call<List<ModelTotalSpecificBank>> view_total_specific_bank(@Query("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_transaction.php")
    Call<ResponseBody> add_transaction(@Field("col_card") String col_card,
                                       @Field("col_amount") String col_amount,
                                       @Field("col_type") String col_type,
                                       @Field("col_desc") String col_desc,
                                       @Field("col_date") String col_date,
                                       @Field("col_user_email") String col_user_email);


    @FormUrlEncoded
    @POST("add_transaction_kas.php")
    Call<ResponseBody> add_transaction_kas(@Field("col_type_trans") String col_type_trans,
                                           @Field("col_amount") String col_amount,
                                           @Field("col_type") String col_type,
                                           @Field("col_note") String col_note,
                                           @Field("col_date") String col_date,
                                           @Field("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_transaction_bank.php")
    Call<ResponseBody> add_transaction_bank(@Field("col_type_trans") String col_type_trans,
                                            @Field("col_amount") String col_amount,
                                            @Field("col_type") String col_type,
                                            @Field("col_bank_name") String col_bank_name,
                                            @Field("col_note") String col_note,
                                            @Field("col_date") String col_date,
                                            @Field("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_transaction_wallet.php")
    Call<ResponseBody> add_transaction_wallet(@Field("col_type_trans") String col_type_trans,
                                              @Field("col_amount") String col_amount,
                                              @Field("col_type") String col_type,
                                              @Field("col_note") String col_note,
                                              @Field("col_date") String col_date,
                                              @Field("col_user_email") String col_user_email);

    @GET("view_transaction.php")
    Call<List<ModelTransaction>> view_transaction(@Query("col_user_email") String col_user_email);

    @GET("view_request.php")
    Call<ArrayList<ModelRequest>> view_request(@Query("col_user_email") String col_user_email);

    @GET("view_type_trans.php")
    Call<List<ModelTypeTrans>> view_type_trans();

    @GET("view_history_filter.php")
    Call<List<ModelTransaction>> view_history_filter(@Query("col_user_email") String col_user_email,
                                                     @Query("col_type") String col_type,
                                                     @Query("col_date_start") String col_date_start,
                                                     @Query("col_date_end") String col_date_end);

    @FormUrlEncoded
    @POST("add_customer.php")
    Call<ResponseBody> add_customer(@Field("col_customer_name") String col_customer_name,
                                    @Field("col_customer_email") String col_customer_email,
                                    @Field("col_number_phone") String col_number_phone,
                                    @Field("col_address") String col_address,
                                    @Field("col_user_email") String col_user_email);

    @GET("view_customer.php")
    Call<List<ModelCustomer>> view_customer(@Query("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_transaction_debt.php")
    Call<ResponseBody> add_transaction_debt(@Field("col_type") String col_type,
                                            @Field("col_type_trans") String col_type_trans,
                                            @Field("col_person") String col_person,
                                            @Field("col_via") String col_via,
                                            @Field("col_bank_name") String col_bank_name,
                                            @Field("col_amount") String col_amount,
                                            @Field("col_date") String col_date,
                                            @Field("col_date_due") String col_date_due,
                                            @Field("col_note") String col_note,
                                            @Field("col_status") String col_status,
                                            @Field("col_ref") String col_ref,
                                            @Field("col_user_email") String col_user_email);

    @GET("view_transaction_debt.php")
    Call<List<ModelTransactionDebt>> view_trans_debt(@Query("col_user_email") String col_user_email);

    @GET("view_total_debt.php")
    Call<List<ModelTransactionDebt>> view_total_debt(@Query("col_user_email") String col_user_email);

    @GET("view_debt_detail.php")
    Call<List<ModelTransactionDebt>> view_debt_detail(@Query("col_person") String col_person,
                                                      @Query("col_user_email") String col_user_email);

    @GET("view_total_debt_specific.php")
    Call<List<ModelTransactionDebt>> view_total_debt_specific(@Query("col_debt_type") String col_debt_type,
                                                              @Query("col_user_email") String col_user_email);

    @GET("view_total_debt_specific_person.php")
    Call<List<ModelTransactionDebt>> view_total_debt_specific_person(@Query("col_person") String col_person,
                                                                     @Query("col_user_email") String col_user_email);

    @GET("view_report_transaction.php")
    Call<List<ModelTransaction>> view_report_transaction(@Query("col_type_trans") String col_type_trans,
                                                         @Query("col_user_email") String col_user_email);

    @FormUrlEncoded
    @POST("add_debt.php")
    Call<ModelDebt> add_debt(@Field("debtor") String debtor,
                             @Field("creditor") String creditor,
                             @Field("amount") String amount,
                             @Field("note") String note,
                             @Field("start_date") String start_date,
                             @Field("due_date") String due_date,
                             @Field("created_by") String created_by,
                             @Field("created_date") String created_date);


    @GET("view_list_debt.php")
    Call<ModelDebt> view_list_debt(@Query("debtor") String debtor,
                                   @Query("start_date") String start_date,
                                   @Query("end_date") String end_date,
                                   @Query("user_email") String user_email,
                                   @Query("display") String display);

    @GET("view_total_amount_debt.php")
    Call<ModelDebt> view_total_amount_debt(@Query("debtor") String debtor,
                                           @Query("user_email") String user_email,
                                           @Query("start_date") String start_date,
                                           @Query("end_date") String end_date,
                                           @Query("display") String display);

    @GET("view_detail_list_debt.php")
    Call<ModelDebt> view_detail_list_debt(@Query("debtor") String debtor,
                                          @Query("creditor") String creditor,
                                          @Query("start_date") String start_date,
                                          @Query("end_date") String end_date,
                                          @Query("user_email") String user_email,
                                          @Query("display") String display);

}
