/*
 * Keuanganku
 * CardAdapter.java
 *
 * Created by Triono Hidayat on 4/20/22, 3:33 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 3:33 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    List<ModelCard> modelCardArrayList;

    public CardAdapter(List<ModelCard> modelCardArrayList) {
        this.modelCardArrayList = modelCardArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelCard modelCard = modelCardArrayList.get(position);

        holder.txtNameBank.setText(modelCard.getColNameBank());
        holder.txtOwner.setText(modelCard.getColNameOwner());
        holder.txtNumber.setText(modelCard.getColAccountNumber());
        holder.txtAmount.setText(formatRupiah(Double.parseDouble(modelCard.getAmountTotal())));

        if (holder.txtNameBank.getText().equals("BANK BRI")) {
            holder.imgBank.setImageResource(R.drawable.bank_bri);
        } else if (holder.txtNameBank.getText().equals("BANK BNI")) {
            holder.imgBank.setImageResource(R.drawable.bank_bni);
        } else if (holder.txtNameBank.getText().equals("BANK BTN")) {
            holder.imgBank.setImageResource(R.drawable.bank_btn);
        } else {
            holder.imgBank.setImageResource(R.drawable.bank_indonesia);
        }
    }

    @Override
    public int getItemCount() {
        return modelCardArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgBank;
        TextView txtNameBank, txtOwner, txtNumber, txtAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgBank = itemView.findViewById(R.id.img_card_logo);
            txtNameBank = itemView.findViewById(R.id.text_card_name);
            txtOwner = itemView.findViewById(R.id.text_card_owner);
            txtNumber = itemView.findViewById(R.id.text_card_number);
            txtAmount = itemView.findViewById(R.id.text_card_amount);

        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
