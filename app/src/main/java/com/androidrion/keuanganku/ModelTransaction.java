/*
 * Keuanganku
 * ModelTransaction.java
 *
 * Created by Triono Hidayat on 4/20/22, 5:46 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 5:46 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTransaction {

    @SerializedName("col_type_trans")
    @Expose
    private String colTypeTrans;
    @SerializedName("col_amount")
    @Expose
    private String colAmount;
    @SerializedName("col_type")
    @Expose
    private String colType;
    @SerializedName("col_bank_name")
    @Expose
    private String colBankName;
    @SerializedName("col_note")
    @Expose
    private String colNote;
    @SerializedName("col_date")
    @Expose
    private String colDate;
    @SerializedName("col_user_email")
    @Expose
    private String colUserEmail;
    @SerializedName("col_create")
    @Expose
    private String colCreate;

    @SerializedName("col_total_amount")
    @Expose
    private String colTotalAmount;

    /**
     * No args constructor for use in serialization
     *
     */
    public ModelTransaction() {
    }

    /**
     *
     * @param colAmount
     * @param colUserEmail
     * @param colType
     * @param colBankName
     * @param colCreate
     * @param colNote
     * @param colDate
     * @param colTypeTrans
     */
    public ModelTransaction(String colTypeTrans, String colAmount, String colType, String colBankName, String colNote, String colDate, String colUserEmail, String colCreate, String colTotalAmount) {
        super();
        this.colTypeTrans = colTypeTrans;
        this.colAmount = colAmount;
        this.colType = colType;
        this.colBankName = colBankName;
        this.colNote = colNote;
        this.colDate = colDate;
        this.colUserEmail = colUserEmail;
        this.colCreate = colCreate;
        this.colTotalAmount = colTotalAmount;
    }

    public String getColTypeTrans() {
        return colTypeTrans;
    }

    public void setColTypeTrans(String colTypeTrans) {
        this.colTypeTrans = colTypeTrans;
    }

    public String getColAmount() {
        return colAmount;
    }

    public void setColAmount(String colAmount) {
        this.colAmount = colAmount;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColBankName() {
        return colBankName;
    }

    public void setColBankName(String colBankName) {
        this.colBankName = colBankName;
    }

    public String getColNote() {
        return colNote;
    }

    public void setColNote(String colNote) {
        this.colNote = colNote;
    }

    public String getColDate() {
        return colDate;
    }

    public void setColDate(String colDate) {
        this.colDate = colDate;
    }

    public String getColUserEmail() {
        return colUserEmail;
    }

    public void setColUserEmail(String colUserEmail) {
        this.colUserEmail = colUserEmail;
    }

    public String getColCreate() {
        return colCreate;
    }

    public void setColCreate(String colCreate) {
        this.colCreate = colCreate;
    }

    public String getColTotalAmount() {
        return colTotalAmount;
    }

    public void setColTotalAmount(String colTotalAmount) {
        this.colTotalAmount = colTotalAmount;
    }

}