/*
 * Keuanganku
 * ModelCard.java
 *
 * Created by Triono Hidayat on 4/20/22, 3:29 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 3:29 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelCard {

    @SerializedName("col_name_bank")
    @Expose
    private String colNameBank;
    @SerializedName("col_account_number")
    @Expose
    private String colAccountNumber;
    @SerializedName("col_name_owner")
    @Expose
    private String colNameOwner;
    @SerializedName("amount_total")
    @Expose
    private String amountTotal;
    @SerializedName("col_amount")
    @Expose
    private String colAmount;

    public String getColNameBank() {
        return colNameBank;
    }

    public void setColNameBank(String colNameBank) {
        this.colNameBank = colNameBank;
    }

    public String getColAccountNumber() {
        return colAccountNumber;
    }

    public void setColAccountNumber(String colAccountNumber) {
        this.colAccountNumber = colAccountNumber;
    }

    public String getColNameOwner() {
        return colNameOwner;
    }

    public void setColNameOwner(String colNameOwner) {
        this.colNameOwner = colNameOwner;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getColAmount() {
        return colAmount;
    }

    public void setColAmount(String colAmount) {
        this.colAmount = colAmount;
    }

}
