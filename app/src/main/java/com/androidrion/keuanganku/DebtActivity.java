/*
 * Keuanganku
 * DebtActivity.java
 *
 * Created by Triono Hidayat on 5/23/22, 3:40 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/23/22, 3:40 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DebtActivity extends AppCompatActivity {

    //User Email
    SharedPrefManager sharedPrefManager;

    Calendar calendar;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat;

    //API
    API_Interface apiInterface;

    //Activity Component
    SwipeRefreshLayout refreshLayout;
    //    TextView textDebtIn, textDebtOut;
//    RecyclerView rvDebt;
    FloatingActionButton fabAddDebt, fabAddDebtPerson, fabAddDebtNote;
    View backDrop, lytAddDebtPerson, lytAddDebtNote;

    TabLayout tabLayout;
    ViewPager viewPager;
    DebtViewPagerAdapter debtViewPagerAdapter;

    //Adapter
    TransactionDebtMeAdapter debtAdapter;

    //Dialog Add Debt Note Component
    AutoCompleteTextView inputDebtName;
    RadioGroup radioGroup;
    RadioButton rbPrivate, rbCustomer;
    TextInputLayout layoutDebtName;
    TextInputEditText inputDebtAmount, inputDebtDate, inputDebtDateDue, inputDebtNote;
    MaterialCardView cardDebtSave;

    //Dialog Add Debt Person Component
    TextInputEditText inputName, inputEmail, inputPhone, inputAddress;
    MaterialCardView cardAdd;

    //String Dialog Component
    String sType, sCustomer, sAmount, sDate, sDateDue, sNote;

    //String Dialog Add Customer
    String sName, sEmail, sPhone, sAddress;

    //List
    List<ModelTransactionDebt> transactionDebts;
    List<ModelCustomer> customers;
    ArrayAdapter<String> adapter;
    List<ModelCard> modelCards;

    //Adapter
    CustomerAdapter customerAdapter;
    DebtBankAdapter debtBankAdapter;

    private boolean rotate = false;

    //Format Rupiah
    BigDecimal value;
    String rupiah = "0";

    String status, message;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Catatan Utang");

        sharedPrefManager = new SharedPrefManager(this);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

        apiInterface = API_Client.getClient().create(API_Interface.class);

        refreshLayout = findViewById(R.id.swipe_refresh);
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshLayout.setOnRefreshListener(() -> {
            refreshLayout.setRefreshing(false);
        });

        tabLayout = findViewById(R.id.tab_layout);

        debtViewPagerAdapter = new DebtViewPagerAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.pager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setAdapter(debtViewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fabAddDebt = findViewById(R.id.fab_add_debt);
        fabAddDebtPerson = findViewById(R.id.fab_add_debt_person);
        fabAddDebtNote = findViewById(R.id.fab_add_debt_note);

        backDrop = findViewById(R.id.back_drop);
        lytAddDebtPerson = findViewById(R.id.lyt_add_debt_person);
        lytAddDebtNote = findViewById(R.id.lyt_add_debt_note);
        ViewAnimation.initShowOut(lytAddDebtPerson);
        ViewAnimation.initShowOut(lytAddDebtNote);
        backDrop.setVisibility(View.GONE);

        fabAddDebt.setOnClickListener(this::toggleFabMode);

        fabAddDebtPerson.setOnClickListener(view -> showDialogAddCustomer());
        fabAddDebtNote.setOnClickListener(view -> showDialogDebt());

        backDrop.setOnClickListener(v -> toggleFabMode(fabAddDebt));

    }


    private void showDialogDebt() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        radioGroup = dialog.findViewById(R.id.group_for);
        rbPrivate = dialog.findViewById(R.id.radio_private);
        rbCustomer = dialog.findViewById(R.id.radio_customer);

        layoutDebtName = dialog.findViewById(R.id.layout_debt_name);
        inputDebtName = dialog.findViewById(R.id.input_debt_name);
        inputDebtAmount = dialog.findViewById(R.id.input_debt_amount);
        inputDebtDate = dialog.findViewById(R.id.input_debt_date);
        inputDebtDateDue = dialog.findViewById(R.id.input_debt_due_date);
        inputDebtNote = dialog.findViewById(R.id.input_debt_note);
        cardDebtSave = dialog.findViewById(R.id.card_debt_save);

        radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();

            if (selectedId == rbPrivate.getId()) {
                layoutDebtName.setVisibility(View.VISIBLE);
                layoutDebtName.setHint("Pemberi");
            } else if (selectedId == rbCustomer.getId()) {
                layoutDebtName.setVisibility(View.VISIBLE);
                layoutDebtName.setHint("Pelanggan");
            }

            Call<List<ModelCustomer>> callViewCustomer = apiInterface.view_customer(sharedPrefManager.getSPEmail());
            callViewCustomer.enqueue(new Callback<List<ModelCustomer>>() {
                @Override
                public void onResponse(Call<List<ModelCustomer>> call, Response<List<ModelCustomer>> response) {

                    customers = response.body();
                    List<String> listAuto = new ArrayList<>();

                    for (int i = 0; i < Objects.requireNonNull(customers).size(); i++) {
                        listAuto.add(customers.get(i).getColCustomerName());

                    }

                    adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                    inputDebtName.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<List<ModelCustomer>> call, Throwable t) {

                }
            });

        });

        inputDebtAmount.addTextChangedListener(new MoneyTextWatcher(inputDebtAmount));

        inputDebtDate.setOnClickListener(view -> showDialogDate());
        inputDebtDateDue.setOnClickListener(view -> showDialogDateDue());

        cardDebtSave.setOnClickListener(view -> {
            sCustomer = inputDebtName.getText().toString();

            value = MoneyTextWatcher.parseCurrencyValue(inputDebtAmount.getText().toString());
            sAmount = value.toPlainString();
//            sAmount = inputDebtAmount.getText().toString();

            sDate = inputDebtDate.getText().toString();
            sDateDue = inputDebtDateDue.getText().toString();
            sNote = inputDebtNote.getText().toString();

            if (radioGroup.getCheckedRadioButtonId() == -1) {
                Toast.makeText(this, "Anda belum memilih utang untuk siapa", Toast.LENGTH_SHORT).show();
            } else if (sCustomer.isEmpty()) {
                Toast.makeText(this, "Mohon pilih Customer", Toast.LENGTH_SHORT).show();
            } else if (sAmount.isEmpty()) {
                Toast.makeText(this, "Mohon masukkan nominal", Toast.LENGTH_SHORT).show();
            } else if (sDate.isEmpty()) {
                Toast.makeText(this, "Pilih Tanggal", Toast.LENGTH_SHORT).show();
            } else if (sDateDue.isEmpty()) {
                Toast.makeText(this, "Pilih Tanggal Jatuh Tempo", Toast.LENGTH_SHORT).show();
            } else if (sNote.isEmpty()) {
                Toast.makeText(this, "Mohon Masukkan Catatan", Toast.LENGTH_SHORT).show();
            } else {

                if (rbPrivate.isChecked()) {
                    Call<ModelDebt> addDebt = apiInterface.add_debt(sharedPrefManager.getSPEmail(), sCustomer, sAmount, sNote, sDate, sDateDue, sharedPrefManager.getSPEmail(), sDate);
                    addDebt.enqueue(new Callback<ModelDebt>() {
                        @Override
                        public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                            if (response.isSuccessful()) {
                                ModelDebt debtResponse = response.body();

                                String message = debtResponse.message;

                                Toast.makeText(DebtActivity.this, message, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<ModelDebt> call, Throwable t) {
                            Log.d("error", t.getMessage());
                        }
                    });
                } else if (rbCustomer.isChecked()) {
                    Call<ModelDebt> addDebt = apiInterface.add_debt(sCustomer, sharedPrefManager.getSPEmail(), sAmount, sNote, sDate, sDateDue, sharedPrefManager.getSPEmail(), sDate);
                    addDebt.enqueue(new Callback<ModelDebt>() {
                        @Override
                        public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                            if (response.isSuccessful()) {
                                ModelDebt debtResponse = response.body();

                                String message = debtResponse.message;

                                Toast.makeText(DebtActivity.this, message, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<ModelDebt> call, Throwable t) {
                            Log.d("error", t.getMessage());
                        }
                    });
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogAddCustomer() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        inputName = dialog.findViewById(R.id.input_cust_name);
        inputEmail = dialog.findViewById(R.id.input_cust_email);
        inputPhone = dialog.findViewById(R.id.input_cust_phone);
        inputAddress = dialog.findViewById(R.id.input_cust_address);

        cardAdd = dialog.findViewById(R.id.card_add);

        cardAdd.setOnClickListener(view -> {

            sName = inputName.getText().toString();
            sEmail = inputEmail.getText().toString();
            sPhone = inputPhone.getText().toString();
            sAddress = inputAddress.getText().toString();

            Call<ResponseBody> addCustomer = apiInterface.add_customer(sName, sEmail, sPhone, sAddress, sharedPrefManager.getSPEmail());
            addCustomer.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {

                        dialog.dismiss();
                    } else {
                        Toast.makeText(DebtActivity.this, "Maaf, coba lagi!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void toggleFabMode(View view) {
        rotate = ViewAnimation.rotateFab(view, !rotate);
        if (rotate) {
            ViewAnimation.showIn(lytAddDebtPerson);
            ViewAnimation.showIn(lytAddDebtNote);
            backDrop.setVisibility(View.VISIBLE);
        } else {
            ViewAnimation.showOut(lytAddDebtPerson);
            ViewAnimation.showOut(lytAddDebtNote);
            backDrop.setVisibility(View.GONE);
        }
    }

    private void showDialogDate() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDate.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void showDialogDateDue() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDateDue.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    private String formatRupiah(Double number) {
//        Locale localeID = new Locale("in", "ID");
//        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
//        return formatRupiah.format(number);
//    }
}