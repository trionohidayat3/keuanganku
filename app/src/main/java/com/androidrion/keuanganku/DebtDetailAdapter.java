/*
 * Keuanganku
 * TransactionDebtAdapter.java
 *
 * Created by Triono Hidayat on 5/24/22, 9:41 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/24/22, 9:41 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class DebtDetailAdapter extends RecyclerView.Adapter<DebtDetailAdapter.ViewHolder> {

    List<ModelDebt.Datum> debtList;

    public DebtDetailAdapter(List<ModelDebt.Datum> debtList) {
        this.debtList = debtList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_debt_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelDebt.Datum modelTransactionDebt = debtList.get(position);

        holder.textDate.setText(modelTransactionDebt.getStartDate());
        holder.textAmount.setText(formatRupiah(Double.parseDouble(modelTransactionDebt.getAmount())));
        holder.textDue.setText(modelTransactionDebt.getDueDate());

        holder.itemView.setOnClickListener(view -> showDialogDebtDetail(holder, position));
    }

    private void showDialogDebtDetail(ViewHolder holder, int position) {
        final Dialog dialog = new Dialog(holder.itemView.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt_detail_specific);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ModelDebt.Datum modelTransactionDebt = debtList.get(position);

        TextView textAmount = dialog.findViewById(R.id.text_debt_detail_amount);
        TextView textNote = dialog.findViewById(R.id.text_debt_detail_note);

        textAmount.setText(formatRupiah(Double.parseDouble(modelTransactionDebt.getAmount())));
        textNote.setText(modelTransactionDebt.getNote());

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public int getItemCount() {
        return debtList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textDate, textNote, textDue, textAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textDate = itemView.findViewById(R.id.text_debt_date);
//            textNote = itemView.findViewById(R.id.text_debt_note);
            textAmount = itemView.findViewById(R.id.text_debt_detail_amount);
            textDue = itemView.findViewById(R.id.text_debt_detail_due);
        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
