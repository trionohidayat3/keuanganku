/*
 * Keuanganku
 * TransactionDebtAdapter.java
 *
 * Created by Triono Hidayat on 5/24/22, 9:41 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/24/22, 9:41 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class TransactionDebtCustAdapter extends RecyclerView.Adapter<TransactionDebtCustAdapter.ViewHolder> {

    List<ModelDebt.Datum> debtList;
    Context mContext;

    public TransactionDebtCustAdapter(List<ModelDebt.Datum> debtList, Context context) {
        this.debtList = debtList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_debt, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelDebt.Datum modelTransactionDebt = debtList.get(position);

        holder.textIntial.setText(modelTransactionDebt.getDebtor().substring(0, 1));
        holder.textName.setText(modelTransactionDebt.getDebtor());
        holder.textAmount.setText(formatRupiah(Double.parseDouble(modelTransactionDebt.getAmount())));
        holder.textType.setText(modelTransactionDebt.getNote());

        holder.itemView.setOnClickListener(view -> {
            Intent intentDetailDebt = new Intent(holder.itemView.getContext(), DebtDetailActivity.class);
            intentDetailDebt.putExtra("DEBTOR", modelTransactionDebt.getDebtor());
            intentDetailDebt.putExtra("CREDITOR", modelTransactionDebt.getCreditor());
            intentDetailDebt.putExtra("TOTAL", modelTransactionDebt.getAmount());
            intentDetailDebt.putExtra("DISPLAY", "0");
            holder.itemView.getContext().startActivity(intentDetailDebt);

        });

    }

    @Override
    public int getItemCount() {
        return debtList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textIntial, textName, textAmount, textType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textIntial = itemView.findViewById(R.id.text_initial);
            textName = itemView.findViewById(R.id.text_debt_name);
            textAmount = itemView.findViewById(R.id.text_debt_amount);
            textType = itemView.findViewById(R.id.text_debt_type);
        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
