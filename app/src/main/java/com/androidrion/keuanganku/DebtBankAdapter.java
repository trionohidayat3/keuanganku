/*
 * Keuanganku
 * DebtBankAdapter.java
 *
 * Created by Triono Hidayat on 6/2/22, 3:20 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 6/2/22, 3:20 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class DebtBankAdapter extends ArrayAdapter<ModelCard> {

    List<ModelCard> cardList;


    public DebtBankAdapter(Context context, List<ModelCard> modelCards) {
        super(context, R.layout.layout_item_debt_bank, modelCards);

        this.cardList = modelCards;
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ModelCard card = cardList.get(position);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_debt_bank, parent, false);

        TextView textBank = view.findViewById(R.id.text_bank_name);
        TextView textAmount = view.findViewById(R.id.text_bank_amount);

        textBank.setText(card.getColNameBank());
        textAmount.setText(formatRupiah(Double.parseDouble(card.getAmountTotal())));

        return view;
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
