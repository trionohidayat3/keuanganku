/*
 * Keuanganku
 * SpendingFragment.java
 *
 * Created by Triono Hidayat on 5/31/22, 9:59 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/31/22, 9:59 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpendingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpendingFragment extends Fragment {



    public SpendingFragment() {
        // Required empty public constructor
    }


    public static Fragment newInstance() {
        SpendingFragment fragment = new SpendingFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_spending, container, false);
    }
}