/*
 * Keuanganku
 * CustomerAdapter.java
 *
 * Created by Triono Hidayat on 5/25/22, 8:57 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/25/22, 8:57 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {

    List<ModelCustomer> list;

    //Dialog
    TextView textInitial, textName, textEmail, textPhone, textAddress;

    public CustomerAdapter(List<ModelCustomer> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_customer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelCustomer modelCustomer = list.get(position);
        holder.textInitial.setText(modelCustomer.getColCustomerName().substring(0, 1));
        holder.textCustomer.setText(modelCustomer.getColCustomerName());

        holder.itemView.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(view.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_detail_customer);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            textInitial = dialog.findViewById(R.id.text_initial);
            textName = dialog.findViewById(R.id.text_customer_name);
            textEmail = dialog.findViewById(R.id.text_customer_email);
            textPhone = dialog.findViewById(R.id.text_customer_phone);
            textAddress = dialog.findViewById(R.id.text_customer_address);

            textInitial.setText(modelCustomer.getColCustomerName().substring(0, 1));
            textName.setText(modelCustomer.getColCustomerName());
            textEmail.setText(modelCustomer.getColCustomerEmail());
            textPhone.setText(modelCustomer.getColNumberPhone());
            textAddress.setText(modelCustomer.getColAddress());

            dialog.show();
            dialog.getWindow().setAttributes(lp);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textInitial, textCustomer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textInitial = itemView.findViewById(R.id.text_initial);
            textCustomer = itemView.findViewById(R.id.text_customer);
        }
    }

    void setFilter(List<ModelCustomer> filterModel) {
        list = new ArrayList<>();
        list.addAll(filterModel);
        notifyDataSetChanged();
    }
}
