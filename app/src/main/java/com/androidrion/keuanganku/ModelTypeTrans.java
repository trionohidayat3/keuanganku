/*
 * Keuanganku
 * ModelTypeTrans.java
 *
 * Created by Triono Hidayat on 5/12/22, 11:11 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/12/22, 11:11 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTypeTrans {

    @SerializedName("col_type")
    @Expose
    private String colType;

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

}
