/*
 * Keuanganku
 * TransactionAdapter.java
 *
 * Created by Triono Hidayat on 4/21/22, 9:59 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/21/22, 9:59 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    List<ModelTransaction> modelTransactionArrayList;

    public TransactionAdapter(List<ModelTransaction> modelTransactionArrayList) {
        this.modelTransactionArrayList = modelTransactionArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelTransaction modelTransaction = modelTransactionArrayList.get(position);

        holder.textAmount.setText(formatRupiah(Double.parseDouble(modelTransaction.getColAmount())));
        holder.textCard.setText(modelTransaction.getColType());
        holder.textNameBank.setText(modelTransaction.getColBankName());
        holder.textDesc.setText(modelTransaction.getColNote());
        holder.textDate.setText(modelTransaction.getColDate());
        holder.textTransType.setText(modelTransaction.getColTypeTrans());

        if (holder.textNameBank.getText().equals("")){
            holder.textNameBank.setVisibility(View.GONE);
        } else {
            holder.textNameBank.setVisibility(View.VISIBLE);
        }

        if (holder.textTransType.getText().equals("out")) {
            holder.textTransType.setText("-");
            holder.cardViewHistory.setCardBackgroundColor(Color.RED);
        } else {
            holder.textTransType.setText("+");
        }

    }

    @Override
    public int getItemCount() {
        return modelTransactionArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        MaterialCardView cardViewHistory;
        TextView textAmount, textCard, textNameBank, textDesc, textDate, textTransType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardViewHistory = itemView.findViewById(R.id.card_view_history);

            textAmount = itemView.findViewById(R.id.text_trans_amount);
            textCard = itemView.findViewById(R.id.text_trans_bank);
            textNameBank = itemView.findViewById(R.id.text_trans_bank_name);
            textDesc = itemView.findViewById(R.id.text_trans_desc);
            textDate = itemView.findViewById(R.id.text_trans_date);
            textTransType = itemView.findViewById(R.id.text_trans_type);
        }
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}
