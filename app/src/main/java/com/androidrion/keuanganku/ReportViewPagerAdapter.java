/*
 * Keuanganku
 * ReportViewPagerAdapter.java
 *
 * Created by Triono Hidayat on 5/31/22, 10:24 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/31/22, 10:24 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

public class ReportViewPagerAdapter extends FragmentPagerAdapter {

    public ReportViewPagerAdapter(
            @NonNull ReportActivity fm) {
        super(fm.getSupportFragmentManager());
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
            fragment = new IncomeFragment();
        else if (position == 1)
            fragment = new SpendingFragment();

        assert fragment != null;
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
            title = "Pemasukan";
        else if (position == 1)
            title = "Pengeluaran";

        return title;
    }
}