/*
 * Keuanganku
 * ModelTotalSpecificBank.java
 *
 * Created by Triono Hidayat on 5/20/22, 1:55 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/20/22, 1:55 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTotalSpecificBank {

    @SerializedName("col_user_email")
    @Expose
    private String colUserEmail;
    @SerializedName("amount_total")
    @Expose
    private String amountTotal;
    @SerializedName("col_bank_name")
    @Expose
    private String colBankName;

    /**
     * No args constructor for use in serialization
     */
    public ModelTotalSpecificBank() {
    }

    /**
     * @param colUserEmail
     * @param colBankName
     * @param amountTotal
     */
    public ModelTotalSpecificBank(String colUserEmail, String amountTotal, String colBankName) {
        super();
        this.colUserEmail = colUserEmail;
        this.amountTotal = amountTotal;
        this.colBankName = colBankName;
    }

    public String getColUserEmail() {
        return colUserEmail;
    }

    public void setColUserEmail(String colUserEmail) {
        this.colUserEmail = colUserEmail;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getColBankName() {
        return colBankName;
    }

    public void setColBankName(String colBankName) {
        this.colBankName = colBankName;
    }

}
