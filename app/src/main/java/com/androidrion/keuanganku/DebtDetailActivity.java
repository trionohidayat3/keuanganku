/*
 * Keuanganku
 * DebtDetailActivity.java
 *
 * Created by Triono Hidayat on 5/24/22, 2:32 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/24/22, 2:32 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DebtDetailActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    Calendar calendar;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat;

    API_Interface apiInterface;

    SwipeRefreshLayout refreshLayout;
    TextView textDebtName, textDebtAmount;
    MaterialButton btnDeptPaid;
    ImageView imageAccept, imageOffer;
    RecyclerView rvDebtDetail;
    MaterialCardView cardDebtOffer, cardDebtGet;

    DebtDetailAdapter adapter;

    //Dialog Component
    TextInputEditText inputDebtAmount, inputDebtDate, inputDebtDateDue, inputDebtNote;
    AutoCompleteTextView autoBankAccount;
    MaterialCardView cardDebtSave;

    //String Component
    String sDebtor, sCreditor, sBankName, sTotal, sDisplay;

    //String Dialog Component
    String sAmount, sDate, sDateDue, sNote;

    ArrayAdapter<String> arrayAdapter;

    List<ModelTransactionDebt> transactionDebtDetail;
    List<ModelDebt> modelDebtList;
    List<ModelCard> modelCards;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_detail);

        Intent intent = getIntent();
        sDebtor = intent.getStringExtra("DEBTOR");
        sCreditor = intent.getStringExtra("CREDITOR");
        sTotal = intent.getStringExtra("TOTAL");
        sDisplay = intent.getStringExtra("DISPLAY");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Catatan Utang");

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        sharedPrefManager = new SharedPrefManager(this);

        apiInterface = API_Client.getClient().create(API_Interface.class);

        refreshLayout = findViewById(R.id.swipe_refresh);
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshLayout.setOnRefreshListener(() -> {
            refreshLayout.setRefreshing(false);
            loadData();
        });

        textDebtName = findViewById(R.id.text_debt_name);

        if (sDisplay.equals("1")) {
            textDebtName.setText("Total Utang " + sCreditor);
        } else if (sDisplay.equals("0")) {
            textDebtName.setText("Total Utang " + sDebtor);
        }

        textDebtAmount = findViewById(R.id.text_debt_amout);
        textDebtAmount.setText(formatRupiah(Double.parseDouble(sTotal)));

        btnDeptPaid = findViewById(R.id.btn_debt_paid);
        btnDeptPaid.setEnabled(!sTotal.equals("0"));

//        btnDeptPaid.setOnClickListener(view -> showDialogDebtPaid());

        imageAccept = findViewById(R.id.image_accept);
        imageOffer = findViewById(R.id.image_offer);

        imageAccept.setTooltipText("Terima pinjaman atau pelunasan piutang");
        imageOffer.setTooltipText("Beri pinjaman atau bayar utang");

        rvDebtDetail = findViewById(R.id.recycler_debt);
        cardDebtOffer = findViewById(R.id.card_debt_offer);
        cardDebtGet = findViewById(R.id.card_debt_get);

        rvDebtDetail.setHasFixedSize(true);
        rvDebtDetail.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvDebtDetail.setLayoutManager(linearLayoutManager);

//        cardDebtOffer.setOnClickListener(view -> showDialogDebtOffer());
//        cardDebtGet.setOnClickListener(view -> showDialogDebtGet());

        loadData();


    }

    private void loadData() {

        if (sDisplay.equals("0")){
            Call<ModelDebt> viewDetailListDebt = apiInterface.view_detail_list_debt(sharedPrefManager.getSPEmail(), sDebtor, "", "", sharedPrefManager.getSPEmail(), sDisplay);
            viewDetailListDebt.enqueue(new Callback<ModelDebt>() {
                @Override
                public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                    if (response.isSuccessful()) {
                        ModelDebt apigsonList = response.body();
                        assert apigsonList != null;
                        List<ModelDebt.Datum> datumList = apigsonList.data;

                        adapter = new DebtDetailAdapter(datumList);
                        rvDebtDetail.setAdapter(adapter);

                    }
                }

                @Override
                public void onFailure(Call<ModelDebt> call, Throwable t) {

                }
            });
        } else if (sDisplay.equals("1")){
            Call<ModelDebt> viewDetailListDebt = apiInterface.view_detail_list_debt(sharedPrefManager.getSPEmail(), sCreditor, "", "", sharedPrefManager.getSPEmail(), sDisplay);
            viewDetailListDebt.enqueue(new Callback<ModelDebt>() {
                @Override
                public void onResponse(Call<ModelDebt> call, Response<ModelDebt> response) {
                    if (response.isSuccessful()) {
                        ModelDebt apigsonList = response.body();
                        assert apigsonList != null;
                        List<ModelDebt.Datum> datumList = apigsonList.data;

                        adapter = new DebtDetailAdapter(datumList);
                        rvDebtDetail.setAdapter(adapter);

                    }
                }

                @Override
                public void onFailure(Call<ModelDebt> call, Throwable t) {

                }
            });
        }


    }

    private void showDialogDebtPaid() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt_paid);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        RadioGroup rgSaving = dialog.findViewById(R.id.group_saving);
        RadioButton rbKas = dialog.findViewById(R.id.radio_kas);
        RadioButton rbBank = dialog.findViewById(R.id.radio_bank);

        TextInputLayout layoutBank = dialog.findViewById(R.id.layout_bank);

        AutoCompleteTextView autoBankAccount = dialog.findViewById(R.id.auto_bank_account);

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccount.setAdapter(arrayAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        rgSaving.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSaving.getCheckedRadioButtonId();

            if (selectedId == rbKas.getId()) {
                layoutBank.setVisibility(View.GONE);
            } else if (selectedId == rbBank.getId()) {
                layoutBank.setVisibility(View.VISIBLE);
            } else {
                layoutBank.setVisibility(View.VISIBLE);
            }
        });

        TextInputEditText textDebtAmount = dialog.findViewById(R.id.input_debt_amount);
        textDebtAmount.setText(formatRupiah(Double.parseDouble(sTotal)));

        MaterialCardView cardDebtPaidSave = dialog.findViewById(R.id.card_debt_save);

        cardDebtPaidSave.setOnClickListener(view -> {

//            String sDatePaid = dateFormat.format(calendar.getTime());
//            sBankName = autoBankAccount.getText().toString();
//
//            if (rbKas.isChecked()) {
//                if (sType.equals("in")) {
//                    Call<ResponseBody> callDebtPaid = apiInterface.add_transaction_debt("out", "Hutang", sDebtor, "Kas", "-", sTotal, sDatePaid, sDatePaid, "Lunas", "0", "1", sharedPrefManager.getSPEmail());
//                    callDebtPaid.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                            loadData();
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                    Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("out", sTotal, "Kas", "Lunas Utang", sDatePaid, sharedPrefManager.getSPEmail());
//                    callTransactionKas.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                } else {
//                    Call<ResponseBody> callDebtPaid = apiInterface.add_transaction_debt("in", "Hutang", sDebtor, "Kas", "-", sTotal, sDatePaid, sDatePaid, "Lunas", "0", "1", sharedPrefManager.getSPEmail());
//                    callDebtPaid.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                            loadData();
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                    Call<ResponseBody> callTransactionKas = apiInterface.add_transaction_kas("in", sTotal, "Kas", "Lunas Utang", sDatePaid, sharedPrefManager.getSPEmail());
//                    callTransactionKas.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                }
//            } else if (rbBank.isChecked()){
//                if (sType.equals("in")) {
//                    Call<ResponseBody> callDebtPaid = apiInterface.add_transaction_debt("out", "Hutang", sDebtor, "Bank", sBankName, sTotal, sDatePaid, sDatePaid, "Lunas", "0", "1", sharedPrefManager.getSPEmail());
//                    callDebtPaid.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                            loadData();
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                } else {
//                    Call<ResponseBody> callDebtPaid = apiInterface.add_transaction_debt("in", "Hutang", sDebtor, "Bank", sBankName, sTotal, sDatePaid, sDatePaid, "Lunas", "0", "1", sharedPrefManager.getSPEmail());
//                    callDebtPaid.enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                            loadData();
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//
//                }
//            }


            btnDeptPaid.setEnabled(false);
            dialog.dismiss();
        });


        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogDebtOffer() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt_detail);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        RadioGroup rgSaving = dialog.findViewById(R.id.group_saving);
        RadioButton rbKas = dialog.findViewById(R.id.radio_kas);
        RadioButton rbBank = dialog.findViewById(R.id.radio_bank);
        TextInputLayout layoutBank = dialog.findViewById(R.id.layout_bank);
        AutoCompleteTextView autoBankAccount = dialog.findViewById(R.id.auto_bank_account);

        rgSaving.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSaving.getCheckedRadioButtonId();

            if (selectedId == rbKas.getId()) {
                layoutBank.setVisibility(View.GONE);
            } else if (selectedId == rbBank.getId()) {
                layoutBank.setVisibility(View.VISIBLE);
            } else {
                layoutBank.setVisibility(View.VISIBLE);
            }
        });

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccount.setAdapter(arrayAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        inputDebtAmount = dialog.findViewById(R.id.input_debt_amount);
        inputDebtDate = dialog.findViewById(R.id.input_debt_date);
        inputDebtDateDue = dialog.findViewById(R.id.input_debt_due_date);
        inputDebtNote = dialog.findViewById(R.id.input_debt_note);

        inputDebtDate.setOnClickListener(view -> showDialogDate());
        inputDebtDateDue.setOnClickListener(view -> showDialogDateDue());

        cardDebtSave = dialog.findViewById(R.id.card_debt_save);
        cardDebtSave.setOnClickListener(view -> {

            sBankName = autoBankAccount.getText().toString();
            sAmount = inputDebtAmount.getText().toString();
            sDate = inputDebtDate.getText().toString();
            sDateDue = inputDebtDateDue.getText().toString();
            sNote = inputDebtNote.getText().toString();

            if (rbKas.isChecked()) {
                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("out", "Hutang", sDebtor, "Kas", "-", sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            } else if (rbBank.isChecked()) {
                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("out", "Hutang", sDebtor, "Bank", sBankName, sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });


        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogDebtGet() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_debt_detail);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        RadioGroup rgSaving = dialog.findViewById(R.id.group_saving);
        RadioButton rbKas = dialog.findViewById(R.id.radio_kas);
        RadioButton rbBank = dialog.findViewById(R.id.radio_bank);
        TextInputLayout layoutBank = dialog.findViewById(R.id.layout_bank);

        autoBankAccount = dialog.findViewById(R.id.auto_bank_account);

        rgSaving.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = rgSaving.getCheckedRadioButtonId();

            if (selectedId == rbKas.getId()) {
                layoutBank.setVisibility(View.GONE);
            } else if (selectedId == rbBank.getId()) {
                layoutBank.setVisibility(View.VISIBLE);
            } else {
                layoutBank.setVisibility(View.VISIBLE);
            }
        });

        Call<List<ModelCard>> callCardUser = apiInterface.view_card(sharedPrefManager.getSPEmail());
        callCardUser.enqueue(new Callback<List<ModelCard>>() {
            @Override
            public void onResponse(Call<List<ModelCard>> call, Response<List<ModelCard>> response) {
                modelCards = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(modelCards).size(); i++) {
                    listAuto.add(modelCards.get(i).getColNameBank());

                }

                arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listAuto);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                autoBankAccount.setAdapter(arrayAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelCard>> call, Throwable t) {

            }
        });

        inputDebtAmount = dialog.findViewById(R.id.input_debt_amount);
        inputDebtDate = dialog.findViewById(R.id.input_debt_date);
        inputDebtDateDue = dialog.findViewById(R.id.input_debt_due_date);
        inputDebtNote = dialog.findViewById(R.id.input_debt_note);

        inputDebtDate.setOnClickListener(view -> showDialogDate());
        inputDebtDateDue.setOnClickListener(view -> showDialogDateDue());

        cardDebtSave = dialog.findViewById(R.id.card_debt_save);
        cardDebtSave.setOnClickListener(view -> {

            sBankName = autoBankAccount.getText().toString();
            sAmount = inputDebtAmount.getText().toString();
            sDate = inputDebtDate.getText().toString();
            sDateDue = inputDebtDateDue.getText().toString();
            sNote = inputDebtNote.getText().toString();

            if (rbKas.isChecked()) {
                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("in", "Hutang", sDebtor, "Kas", "-", sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            } else if (rbBank.isChecked()) {
                Call<ResponseBody> callAddDebt = apiInterface.add_transaction_debt("in", "Hutang", sDebtor, "Bank", sBankName, sAmount, sDate, sDateDue, sNote, "1", "1", sharedPrefManager.getSPEmail());
                callAddDebt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            loadData();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showDialogDate() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDate.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void showDialogDateDue() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view1, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            inputDebtDateDue.setText(dateFormat.format(newDate.getTime()));


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private String formatRupiah(Double number) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}