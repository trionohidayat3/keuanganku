/*
 * Keuanganku
 * ModelBank.java
 *
 * Created by Triono Hidayat on 4/20/22, 1:34 PM
 *      Email    : halo@trionohidayat.com
 *  Last modified 4/20/22, 12:55 PM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ModelBank {

    @SerializedName("col_name_bank")
    @Expose
    private String colNameBank;

    @SerializedName("col_logo_bank")
    @Expose
    private String colLogoBank;

    public String getColNameBank() {
        return colNameBank;
    }

    public void setColNameBank(String colNameBank) {
        this.colNameBank = colNameBank;
    }

    public String getColLogoBank() {
        return colLogoBank;
    }

    public void setColLogoBank(String colLogoBank) {
        this.colLogoBank = colLogoBank;
    }

}
