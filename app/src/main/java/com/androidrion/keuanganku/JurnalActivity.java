/*
 * Keuanganku
 * JurnalActivity.java
 *
 * Created by Triono Hidayat on 5/20/22, 11:18 AM
 *      Email    : halo@trionohidayat.com
 *  Last modified 5/20/22, 11:18 AM
 *  Copyright (c) 2022.
 *  All rights reserved.
 */

package com.androidrion.keuanganku;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class JurnalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jurnal);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Jurnal");

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}